/*
 * Mapping.java
 *
 * Created on 12 May 2000, 21:08
    Extended by Stas Shvetsov, 2016-2017
 */
import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;

import javax.swing.UIManager.*;

/** 
 *
 * @author  aak97, contributor - stas shvetsov 
 * @version 
 */
public class Mapping extends Object implements CodonConstants
{
  
  /* default mapping from paper */
  
  static MusicalNote  nucA; 
  static MusicalNote  nucT; 
  static MusicalNote  nucG; 
  static MusicalNote  nucC;
  
  static MusicalNote  polar; 
  static MusicalNote  hydrophobic; 
  static MusicalNote  charged;
  static MusicalNote  positive;
  static MusicalNote  aliphatic; 
  static MusicalNote  aromatic;
  static OctaveChange tiny;
  static OctaveChange small;
  static OctaveChange large;

  static int majorOrMinor = 0;
  static int key = 1;
  static int wholeON = 0;
  
  static int nucANoteIndex = 0;
  static int nucAOctaveIndex = 5;
  static int nucCNoteIndex = 10;
  static int nucCOctaveIndex = 5;
  static int nucGNoteIndex = 3;
  static int nucGOctaveIndex = 5;
  static int nucTNoteIndex = 7;
  static int nucTOctaveIndex = 5;
  
  static int polarNoteIndex = 0;
  static int polarOctaveIndex = 3;
  static int hydroNoteIndex = 3;
  static int hydroOctaveIndex = 3;
  static int chargedNoteIndex = 8;
  static int chargedOctaveIndex = 3;
  static int positiveNoteIndex = 7;
  static int positiveOctaveIndex = 3;
  static int aliphaticNoteIndex = 10;
  static int aliphaticOctaveIndex = 3;
  static int aromaticNoteIndex = 5;
  static int aromaticOctaveIndex = 3;
  
  static int suggestedMappingIndex = 0;
  private int generalCodonTime = 3000;
  
  public static Codon[] codons;
  public String[] dummyStrings = new String[6];

  /** Creates new Mapping */
  
  public void Mapping() 
  {
  }
  
  public void defaultOctave()
  {
      nucAOctaveIndex = 5; polarOctaveIndex = 3;
      nucCOctaveIndex = 5; hydroOctaveIndex = 3;
      nucGOctaveIndex = 5; chargedOctaveIndex = 3;
      nucTOctaveIndex = 5; positiveOctaveIndex = 3;
                           aliphaticOctaveIndex = 3;
                           aromaticOctaveIndex = 3;                             
  }
  // Neutral key signature, no sharps or flats
  public void CMajor()
  {
    nucA        = new MusicalNote("A5"); /*  A4  */ nucANoteIndex = 0; 
    nucT        = new MusicalNote("E5"); /*  E3  */ nucCNoteIndex = 7; 
    nucG        = new MusicalNote("C5"); /*  C3  */ nucGNoteIndex = 3; 
    nucC        = new MusicalNote("G5"); /*  G3  */ nucTNoteIndex = 10; 
  
    polar       = new MusicalNote("A3"); /*  A2  */ polarNoteIndex = 0; 
    hydrophobic = new MusicalNote("C3"); /*  C1  */ hydroNoteIndex = 3; 
    charged     = new MusicalNote("F3"); /*  F1  */ chargedNoteIndex = 8; 
    positive    = new MusicalNote("E3"); /*  E1  */ positiveNoteIndex = 7; 
    aliphatic   = new MusicalNote("G3"); /*  G1  */ aliphaticNoteIndex = 10; 
    aromatic    = new MusicalNote("D3"); /*  D1  */ aromaticNoteIndex = 5; 
    
    defaultOctave();
  }
  
  // Two sharp notes, F and C
  public void DMajor()
  {
    nucA        = new MusicalNote("A5"); /*  A4  */ nucANoteIndex = 0; 
    nucT        = new MusicalNote("E5"); /*  E3  */ nucCNoteIndex = 7; 
    nucG        = new MusicalNote("c5"); /*  C Sharp 3  */ nucGNoteIndex = 4; 
    nucC        = new MusicalNote("G5"); /*  G3  */ nucTNoteIndex = 10; 
  
    polar       = new MusicalNote("A3"); /*  A2  */ polarNoteIndex = 0;
    hydrophobic = new MusicalNote("c3"); /*  C Sharp 1  */ hydroNoteIndex = 4;
    charged     = new MusicalNote("f3"); /*  F Sharp 1  */ chargedNoteIndex = 9;
    positive    = new MusicalNote("E3"); /*  E1  */ positiveNoteIndex = 7;
    aliphatic   = new MusicalNote("G3"); /*  G1  */ aliphaticNoteIndex = 10;
    aromatic    = new MusicalNote("D3"); /*  D1  */ aromaticNoteIndex = 5;
    defaultOctave();
  }
  
  // Four sharp notes, F, C, G and D
  public void EMajor()
  {
    nucA        = new MusicalNote("A5"); /*  A4  */ nucANoteIndex = 0; 
    nucT        = new MusicalNote("E5"); /*  E3  */ nucCNoteIndex = 7; 
    nucG        = new MusicalNote("c5"); /*  C Sharp 3  */ nucGNoteIndex = 4; 
    nucC        = new MusicalNote("g5"); /*  G Sharp 3  */ nucTNoteIndex = 11;
  
    polar       = new MusicalNote("A3"); /*  A2  */ polarNoteIndex = 0;
    hydrophobic = new MusicalNote("c3"); /*  C Sharp 1  */ hydroNoteIndex = 4;
    charged     = new MusicalNote("f3"); /*  F Sharp 1  */ chargedNoteIndex = 9;
    positive    = new MusicalNote("E3"); /*  E1  */ positiveNoteIndex = 7; 
    aliphatic   = new MusicalNote("g3"); /*  G Sharp 1  */ aliphaticNoteIndex = 11;
    aromatic    = new MusicalNote("d3"); /*  D Sharp 1  */ aromaticNoteIndex = 6;
    defaultOctave();
  }
  
  // One flat note, B 
  public void FMajor()
  {
    nucA        = new MusicalNote("a5"); /*  A Sharp 4/B Flat 4  */ nucANoteIndex = 1;
    nucT        = new MusicalNote("E5"); /*  E3  */ nucCNoteIndex = 7;
    nucG        = new MusicalNote("C5"); /*  C3  */ nucGNoteIndex = 3;
    nucC        = new MusicalNote("G5"); /*  G3  */ nucTNoteIndex = 10;
  
    polar       = new MusicalNote("a3"); /*  A Sharp 2/B Flat 2  */ polarNoteIndex = 1;
    hydrophobic = new MusicalNote("C3"); /*  C1  */ hydroNoteIndex = 3;
    charged     = new MusicalNote("F3"); /*  F1  */ chargedNoteIndex = 8;
    positive    = new MusicalNote("E3"); /*  E1  */ positiveNoteIndex = 7;
    aliphatic   = new MusicalNote("G3"); /*  G1  */ aliphaticNoteIndex = 10;
    aromatic    = new MusicalNote("D3"); /*  D1  */ aromaticNoteIndex = 5;
    defaultOctave();
  }
  
  // One sharp note, F
  public void GMajor()
  {
    nucA        = new MusicalNote("A5"); /*  A4  */ nucANoteIndex = 0;
    nucT        = new MusicalNote("E5"); /*  E3  */ nucCNoteIndex = 7;
    nucG        = new MusicalNote("C5"); /*  C3  */ nucGNoteIndex = 3;
    nucC        = new MusicalNote("G5"); /*  G3  */ nucTNoteIndex = 10;
  
    polar       = new MusicalNote("A3"); /*  A2  */ polarNoteIndex = 0;
    hydrophobic = new MusicalNote("C3"); /*  C1  */ hydroNoteIndex = 3;
    charged     = new MusicalNote("f3"); /*  F Sharp 1  */ chargedNoteIndex = 9;
    positive    = new MusicalNote("E3"); /*  E1  */ positiveNoteIndex = 7;
    aliphatic   = new MusicalNote("G3"); /*  G1  */ aliphaticNoteIndex = 10;
    aromatic    = new MusicalNote("D3"); /*  D1  */ aromaticNoteIndex = 5;
    defaultOctave();
  }
  
  // Three sharp notes, F, C and G
  public void AMajor()
  {
    nucA        = new MusicalNote("A5"); /*  A4  */ nucANoteIndex = 0;
    nucT        = new MusicalNote("E5"); /*  E3  */ nucCNoteIndex = 7;
    nucG        = new MusicalNote("c5"); /*  C Sharp 3  */ nucGNoteIndex = 4;
    nucC        = new MusicalNote("g5"); /*  G Sharp 3  */ nucTNoteIndex = 11;
  
    polar       = new MusicalNote("A3"); /*  A2  */ polarNoteIndex = 0;
    hydrophobic = new MusicalNote("c3"); /*  C Sharp 1  */ hydroNoteIndex = 4;
    charged     = new MusicalNote("f3"); /*  F Sharp 1  */ chargedNoteIndex = 9;
    positive    = new MusicalNote("E3"); /*  E1  */ positiveNoteIndex = 7;
    aliphatic   = new MusicalNote("g3"); /*  G Sharp 1  */ aliphaticNoteIndex = 11;
    aromatic    = new MusicalNote("D3"); /*  D1  */ aromaticNoteIndex = 5;
    defaultOctave();
  }
  
  // Five sharp notes, F, C, G, D and A
  public void BMajor()
  {
    nucA        = new MusicalNote("a5"); /*  A Sharp 4  */ nucANoteIndex = 1;
    nucT        = new MusicalNote("E5"); /*  E3  */ nucCNoteIndex = 7;
    nucG        = new MusicalNote("c5"); /*  C Sharp 3  */ nucGNoteIndex = 4;
    nucC        = new MusicalNote("g5"); /*  G Sharp 3  */ nucTNoteIndex = 11;
  
    polar       = new MusicalNote("a3"); /*  A Sharp 2  */ polarNoteIndex = 1;
    hydrophobic = new MusicalNote("c3"); /*  C Sharp 1  */ hydroNoteIndex = 4;
    charged     = new MusicalNote("f3"); /*  F Sharp 1  */ chargedNoteIndex = 9;
    positive    = new MusicalNote("E3"); /*  E1  */ positiveNoteIndex = 7;
    aliphatic   = new MusicalNote("g3"); /*  G Sharp 1  */ aliphaticNoteIndex = 11;
    aromatic    = new MusicalNote("d3"); /*  D Sharp 1  */ aromaticNoteIndex = 6;
    defaultOctave();
  }
  
  // Three flat notes, B, E and A
  public void CMinor()
  {
    nucA        = new MusicalNote("a5"); /*  A Sharp 4/B Flat 4  */ nucANoteIndex = 1;
    nucT        = new MusicalNote("E5"); /*  E3  */ nucCNoteIndex = 7;
    nucG        = new MusicalNote("C5"); /*  C3  */ nucGNoteIndex = 3;
    nucC        = new MusicalNote("g5"); /*  G Sharp 3/A Flat 3  */ nucTNoteIndex = 11;
  
    polar       = new MusicalNote("a3"); /*  A Sharp 2/B Flat 2  */ polarNoteIndex = 1;
    hydrophobic = new MusicalNote("C3"); /*  C1  */ hydroNoteIndex = 3;
    charged     = new MusicalNote("F3"); /*  F1  */ chargedNoteIndex = 8;
    positive    = new MusicalNote("E3"); /*  E1  */ positiveNoteIndex = 7;
    aliphatic   = new MusicalNote("g3"); /*  G Sharp 1/A Flat 1  */ aliphaticNoteIndex = 11;
    aromatic    = new MusicalNote("d3"); /*  D Sharp 1/E Flat 1  */ aromaticNoteIndex = 6;
    defaultOctave();
  }
  
  // One flat note, B
  public void DMinor()
  {
    nucA        = new MusicalNote("a5"); /*  A Sharp 4/B Flat 4  */ nucANoteIndex = 1;
    nucT        = new MusicalNote("E5"); /*  E3  */ nucCNoteIndex = 7;
    nucG        = new MusicalNote("C5"); /*  C3  */ nucGNoteIndex = 3;
    nucC        = new MusicalNote("G5"); /*  G3  */ nucTNoteIndex = 10;
  
    polar       = new MusicalNote("a3"); /*  A Sharp 2/B Flat 2  */ polarNoteIndex = 1;
    hydrophobic = new MusicalNote("C3"); /*  C1  */ hydroNoteIndex = 3;
    charged     = new MusicalNote("F3"); /*  F1  */ chargedNoteIndex = 8;
    positive    = new MusicalNote("E3"); /*  E1  */ positiveNoteIndex = 7;
    aliphatic   = new MusicalNote("G3"); /*  G1  */ aliphaticNoteIndex = 10;
    aromatic    = new MusicalNote("D3"); /*  D1  */ aromaticNoteIndex = 5;
    defaultOctave();
  }
  
  // One sharp note, F
  public void EMinor()
  {
    nucA        = new MusicalNote("A5"); /*  A4  */ nucANoteIndex = 0;
    nucT        = new MusicalNote("E5"); /*  E3  */ nucCNoteIndex = 7;
    nucG        = new MusicalNote("C5"); /*  C3  */ nucGNoteIndex = 3;
    nucC        = new MusicalNote("G5"); /*  G3  */ nucTNoteIndex = 10;
  
    polar       = new MusicalNote("A3"); /*  A2  */ polarNoteIndex = 0;
    hydrophobic = new MusicalNote("C3"); /*  C1  */ hydroNoteIndex = 3;
    charged     = new MusicalNote("f3"); /*  F Sharp 1  */ chargedNoteIndex = 9;
    positive    = new MusicalNote("E3"); /*  E1  */ positiveNoteIndex = 7;
    aliphatic   = new MusicalNote("G3"); /*  G1  */ aliphaticNoteIndex = 10;
    aromatic    = new MusicalNote("D3"); /*  D1  */ aromaticNoteIndex = 5;
    defaultOctave();
  }
  
  // Four flat notes, B, E, A and D
  public void FMinor()
  {
    nucA        = new MusicalNote("a5"); /*  A Sharp 4/B Flat 4  */ nucANoteIndex = 1;
    nucT        = new MusicalNote("E5"); /*  E3  */ nucCNoteIndex = 7;
    nucG        = new MusicalNote("c5"); /*  C Sharp 3/D Flat 3  */ nucGNoteIndex = 4;
    nucC        = new MusicalNote("g5"); /*  G Sharp 3/A Flat 3  */ nucTNoteIndex = 11;
  
    polar       = new MusicalNote("a3"); /*  A Sharp 2/B Flat 2  */ polarNoteIndex = 1;
    hydrophobic = new MusicalNote("c3"); /*  C Sharp 1/D Flat 1  */ hydroNoteIndex = 4;
    charged     = new MusicalNote("F3"); /*  F1  */ chargedNoteIndex = 8;
    positive    = new MusicalNote("E3"); /*  E1  */ positiveNoteIndex = 7;
    aliphatic   = new MusicalNote("g3"); /*  G Sharp 1/A Flat 1  */ aliphaticNoteIndex = 11;
    aromatic    = new MusicalNote("d3"); /*  D Sharp 1/E Flat 1  */ aromaticNoteIndex = 6;
    defaultOctave();
  }
  
  // Two flat notes, B and E
  public void GMinor()
  {
    nucA        = new MusicalNote("a5"); /*  A Sharp 4/B Flat 4  */ nucANoteIndex = 1;
    nucT        = new MusicalNote("E5"); /*  E3  */ nucCNoteIndex = 7;
    nucG        = new MusicalNote("C5"); /*  C3  */ nucGNoteIndex = 3;
    nucC        = new MusicalNote("G5"); /*  G3  */ nucTNoteIndex = 10;
  
    polar       = new MusicalNote("a3"); /*  A Sharp 2/B Flat 2  */ polarNoteIndex = 1;
    hydrophobic = new MusicalNote("C3"); /*  C1  */ hydroNoteIndex = 3;
    charged     = new MusicalNote("F3"); /*  F1  */ chargedNoteIndex = 8;
    positive    = new MusicalNote("E3"); /*  E1  */ positiveNoteIndex = 7;
    aliphatic   = new MusicalNote("G3"); /*  G1  */ aliphaticNoteIndex = 10;
    aromatic    = new MusicalNote("d3"); /*  D Sharp 1/E Flat 1  */ aromaticNoteIndex = 6;
    defaultOctave();
  }
  
  // No sharp or flat notes
  public void AMinor()
  {
    nucA        = new MusicalNote("A5"); /*  A4  */ nucANoteIndex = 0; 
    nucT        = new MusicalNote("E5"); /*  E3  */ nucCNoteIndex = 7; 
    nucG        = new MusicalNote("C5"); /*  C3  */ nucGNoteIndex = 3; 
    nucC        = new MusicalNote("G5"); /*  G3  */ nucTNoteIndex = 10; 
  
    polar       = new MusicalNote("A3"); /*  A2  */ polarNoteIndex = 0; 
    hydrophobic = new MusicalNote("C3"); /*  C1  */ hydroNoteIndex = 3; 
    charged     = new MusicalNote("F3"); /*  F1  */ chargedNoteIndex = 8; 
    positive    = new MusicalNote("E3"); /*  E1  */ positiveNoteIndex = 7; 
    aliphatic   = new MusicalNote("G3"); /*  G1  */ aliphaticNoteIndex = 10; 
    aromatic    = new MusicalNote("D3"); /*  D1  */ aromaticNoteIndex = 5; 
    defaultOctave();
  }
  
  // Two sharp notes, F and C
  public void BMinor()
  {
    nucA        = new MusicalNote("A5"); /*  A4  */ nucANoteIndex = 0; 
    nucT        = new MusicalNote("E5"); /*  E3  */ nucCNoteIndex = 7; 
    nucG        = new MusicalNote("c5"); /*  C Sharp 3  */ nucGNoteIndex = 4; 
    nucC        = new MusicalNote("G5"); /*  G3  */ nucTNoteIndex = 10; 
  
    polar       = new MusicalNote("A3"); /*  A2  */ polarNoteIndex = 0;
    hydrophobic = new MusicalNote("c3"); /*  C Sharp 1  */ hydroNoteIndex = 4;
    charged     = new MusicalNote("f3"); /*  F Sharp 1  */ chargedNoteIndex = 9;
    positive    = new MusicalNote("E3"); /*  E1  */ positiveNoteIndex = 7;
    aliphatic   = new MusicalNote("G3"); /*  G1  */ aliphaticNoteIndex = 10;
    aromatic    = new MusicalNote("D3"); /*  D1  */ aromaticNoteIndex = 5;
    defaultOctave();
  }
  
  public void map_1_journey()
  {
        nucA        = new MusicalNote("G3");
        nucT        = new MusicalNote("C3");
        nucG        = new MusicalNote("E3"); 
        nucC        = new MusicalNote("G3");
  
        polar       = new MusicalNote("C5"); 
        hydrophobic = new MusicalNote("F5");
        charged     = new MusicalNote("D5");
        positive    = new MusicalNote("E5");
        aliphatic   = new MusicalNote("A5");
        aromatic    = new MusicalNote("G5");
  }
  
  public void map_2_contemporary()
  {
        nucA        = new MusicalNote("C5");
        nucT        = new MusicalNote("G5");
        nucG        = new MusicalNote("C6"); 
        nucC        = new MusicalNote("E5");
  
        polar       = new MusicalNote("a3"); 
        hydrophobic = new MusicalNote("d3");
        charged     = new MusicalNote("c3");
        positive    = new MusicalNote("f3");
        aliphatic   = new MusicalNote("g4");
        aromatic    = new MusicalNote("B3");
  }
  
  public void map_3_A_flat_major_6()
  {
        nucA        = new MusicalNote("d5");
        nucT        = new MusicalNote("F5");
        nucG        = new MusicalNote("G5"); 
        nucC        = new MusicalNote("g5");
  
        polar       = new MusicalNote("d4"); 
        hydrophobic = new MusicalNote("g4");
        charged     = new MusicalNote("C5");
        positive    = new MusicalNote("F5");
        aliphatic   = new MusicalNote("g6");
        aromatic    = new MusicalNote("C6");
  }
  
  public void map_4_Diminished_D()
  {
        nucA        = new MusicalNote("B5");
        nucT        = new MusicalNote("D6");
        nucG        = new MusicalNote("F6"); 
        nucC        = new MusicalNote("g6");
  
        polar       = new MusicalNote("D4"); 
        hydrophobic = new MusicalNote("F4");
        charged     = new MusicalNote("g4");
        positive    = new MusicalNote("B4");
        aliphatic   = new MusicalNote("D5");
        aromatic    = new MusicalNote("F5");
  }
  
  public void map_5_bass_ostinato()
  {
        nucA        = new MusicalNote("D5");
        nucT        = new MusicalNote("A5");
        nucG        = new MusicalNote("F5"); 
        nucC        = new MusicalNote("C5");
  
        polar       = new MusicalNote("G3"); 
        hydrophobic = new MusicalNote("C3");
        charged     = new MusicalNote("F3");
        positive    = new MusicalNote("D4");
        aliphatic   = new MusicalNote("F2");
        aromatic    = new MusicalNote("F3");
  }
  
  public void map_6_raindrops()
  {
        nucA        = new MusicalNote("d5");
        nucT        = new MusicalNote("f5");
        nucG        = new MusicalNote("a6"); 
        nucC        = new MusicalNote("d6");
  
        polar       = new MusicalNote("d5"); 
        hydrophobic = new MusicalNote("f5");
        charged     = new MusicalNote("g5");
        positive    = new MusicalNote("a5");
        aliphatic   = new MusicalNote("c6");
        aromatic    = new MusicalNote("d6");
  }
  
  public String theNoteToBePlayed (int noteIndex) {
      
      String retNote = "";
      
      if (noteIndex == 0) retNote = "A";
      else if (noteIndex == 1) retNote = "a";
      else if (noteIndex == 2) retNote = "B";
      else if (noteIndex == 3) retNote = "C";
      else if (noteIndex == 4) retNote = "c";
      else if (noteIndex == 5) retNote = "D";
      else if (noteIndex == 6) retNote = "d";
      else if (noteIndex == 7) retNote = "E";
      else if (noteIndex == 8) retNote = "F";
      else if (noteIndex == 9) retNote = "f";
      else if (noteIndex == 10) retNote = "G";
      else if (noteIndex == 11) retNote = "g";
      
      return retNote;
  }
  
   public String theOctaveToBePlayed (int octaveIndex) {
      
      String retOctave = "";
      
      if (octaveIndex == 1) retOctave = "1";
      else if (octaveIndex == 2) retOctave = "2";
      else if (octaveIndex == 3) retOctave = "3";
      else if (octaveIndex == 4) retOctave = "4";
      else if (octaveIndex == 5) retOctave = "5";
      else if (octaveIndex == 6) retOctave = "6";
      else if (octaveIndex == 7) retOctave = "7";
      
      return retOctave;
  }
  
  public void setNucAMapping()
  {
    nucA = new MusicalNote(theNoteToBePlayed(nucANoteIndex)+theOctaveToBePlayed(nucAOctaveIndex)); 
  }
  
  public void setNucCMapping()
  {
    nucC = new MusicalNote(theNoteToBePlayed(nucCNoteIndex)+theOctaveToBePlayed(nucCOctaveIndex));  
  }
  
  public void setNucGMapping()
  {
    nucG = new MusicalNote(theNoteToBePlayed(nucGNoteIndex)+theOctaveToBePlayed(nucGOctaveIndex));   
  }
  
  public void setNucTMapping()
  {
    nucT = new MusicalNote(theNoteToBePlayed(nucTNoteIndex)+theOctaveToBePlayed(nucTOctaveIndex));   
  }
  
  public void setPolarMapping()
  {
    polar = new MusicalNote(theNoteToBePlayed(polarNoteIndex)+theOctaveToBePlayed(polarOctaveIndex)); 
  }
  
  public void setHydrophobicMapping()
  {
    hydrophobic = new MusicalNote(theNoteToBePlayed(hydroNoteIndex)+theOctaveToBePlayed(hydroOctaveIndex));   
  }
  
  public void setChargedMapping()
  {
    charged = new MusicalNote(theNoteToBePlayed(chargedNoteIndex)+theOctaveToBePlayed(chargedOctaveIndex));   
  }
  
  public void setPositiveMapping()
  {
    positive = new MusicalNote(theNoteToBePlayed(positiveNoteIndex)+theOctaveToBePlayed(positiveOctaveIndex));   
  }
  
  public void setAliphaticMapping()
  {
    aliphatic = new MusicalNote(theNoteToBePlayed(aliphaticNoteIndex)+theOctaveToBePlayed(aliphaticOctaveIndex));   
  }
  
  public void setAromaticMapping()
  {
    aromatic = new MusicalNote(theNoteToBePlayed(aromaticNoteIndex)+theOctaveToBePlayed(aromaticOctaveIndex));  
  } 
   
  public void init()
  {
    

    long[]  playTime     = new long[6];
    int     gap          = generalCodonTime/6;
    int[]   durationTime = new int[2];
    int[]   velocityTime = new int[2];

    durationTime[0] = generalCodonTime/3;
    durationTime[1] = durationTime[0]/2;
    velocityTime[0] = 70;
    velocityTime[1] = 100;
      
    playTime[0] = 0;
    for(int i = 1;i<playTime.length;i++)
    {
        playTime[i] = playTime[i-1]+gap;
    }
    
    switch (suggestedMappingIndex) {
        case 1: map_1_journey(); break;
        case 2: map_2_contemporary(); break;
        case 3: map_3_A_flat_major_6(); break;
        case 4: map_4_Diminished_D(); break;
        case 5: map_5_bass_ostinato(); break;
        case 6: map_6_raindrops(); break;
        default: suggestedMappingIndex = 0; break;
    }
    
    setNucAMapping();
    setNucCMapping();
    setNucGMapping();
    setNucTMapping();
    
    setPolarMapping();
    setHydrophobicMapping();
    setChargedMapping();
    setPositiveMapping();
    setAliphaticMapping();
    setAromaticMapping();
    
    if (suggestedMappingIndex == 0 && key != 0)
    {
        switch(majorOrMinor)
        {
            case 0: // MAJOR KEYS
            { 
                switch(key)
                {
                    case 1: CMajor(); break;
                    case 2: DMajor(); break;
                    case 3: EMajor(); break;
                    case 4: FMajor(); break;
                    case 5: GMajor(); break;
                    case 6: AMajor(); break;
                    case 7: BMajor(); break;
                    default: {CMajor(); key = 1;} break;
                }
            } break;
            case 1: // MINOR KEYS
            {
                switch(key)
                {
                    case 1: CMinor(); break;
                    case 2: DMinor(); break;
                    case 3: EMinor(); break;
                    case 4: FMinor(); break;
                    case 5: GMinor(); break;
                    case 6: AMinor(); break;
                    case 7: BMinor(); break;
                    default: {CMinor(); key = 1;} break;
                }
            } break;
            default:
            {
                CMajor();
                majorOrMinor = 0;
            } break;
        }
    }
    
    whole(false);
    
    tiny        = new OctaveChange(2); /*  All plus one ocatve  */
    small       = new OctaveChange(1); /*  All plus one ocatve  */
    large       = new OctaveChange(1); /*  All plus one ocatve  */

    codons = new Codon[CodonConstants.NUMBEROFAMINOACIDS];
    
    dummyStrings[0] = "TTT";
    dummyStrings[1] = "TTC";
    dummyStrings[2] = "";
    dummyStrings[3] = "";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[0] = new Codon(dummyStrings,"f");
    codons[0].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(aromatic),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[0].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(hydrophobic),playTime[4],durationTime[0],velocityTime[1],velocityTime[0])); 
    codons[0].setSize(CodonConstants.LARGE);
        
    dummyStrings[0] = "TTA";
    dummyStrings[1] = "TTG";
    dummyStrings[2] = "CTT";
    dummyStrings[3] = "CTC";
    dummyStrings[4] = "CTA";
    dummyStrings[5] = "CTG";
    codons[1] = new Codon(dummyStrings,"l");
    codons[1].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(aliphatic),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[1].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(hydrophobic),playTime[4],durationTime[0],velocityTime[1],velocityTime[0]));
    codons[1].setSize(CodonConstants.LARGE);
    
    dummyStrings[0] = "ATT";
    dummyStrings[1] = "ATC";
    dummyStrings[2] = "ATA";
    dummyStrings[3] = "";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[2] = new Codon(dummyStrings,"i");
    codons[2].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(hydrophobic),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[2].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(aliphatic),playTime[4],durationTime[0],velocityTime[1],velocityTime[0]));
    codons[2].setSize(CodonConstants.LARGE);
    
    dummyStrings[0] = "ATG";
    dummyStrings[1] = "";
    dummyStrings[2] = "";
    dummyStrings[3] = "";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[3] = new Codon(dummyStrings,"m");
    codons[3].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(hydrophobic),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[3].setSize(CodonConstants.LARGE);
    
    dummyStrings[0] = "GTT";
    dummyStrings[1] = "GTC";
    dummyStrings[2] = "GTA";
    dummyStrings[3] = "GTG";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[4] = new Codon(dummyStrings,"v");
    codons[4].addAminoAcidTiming(new AminoAcidTimeAttributes(small.convertNote(hydrophobic),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[4].addAminoAcidTiming(new AminoAcidTimeAttributes(small.convertNote(aliphatic),playTime[4],durationTime[0],velocityTime[1],velocityTime[0]));
    codons[4].setSize(CodonConstants.SMALL);
    
    dummyStrings[0] = "TCT";
    dummyStrings[1] = "TCC";
    dummyStrings[2] = "TCA";
    dummyStrings[3] = "TCG";
    dummyStrings[4] = "AGT";
    dummyStrings[5] = "AGC";
    codons[5] = new Codon(dummyStrings,"s");
    codons[5].addAminoAcidTiming(new AminoAcidTimeAttributes(tiny.convertNote(polar),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[5].setSize(CodonConstants.TINY);
    
    dummyStrings[0] = "CCT";
    dummyStrings[1] = "CCC";
    dummyStrings[2] = "CCA";
    dummyStrings[3] = "CCG";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[6] = new Codon(dummyStrings,"p");
    codons[6].setSize(CodonConstants.SMALL);
    
    dummyStrings[0] = "ACT";
    dummyStrings[1] = "ACC";
    dummyStrings[2] = "ACA";
    dummyStrings[3] = "ACG";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[7] = new Codon(dummyStrings,"t");
    codons[7].addAminoAcidTiming(new AminoAcidTimeAttributes(small.convertNote(polar),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[7].addAminoAcidTiming(new AminoAcidTimeAttributes(small.convertNote(hydrophobic),playTime[4],durationTime[0],velocityTime[1],velocityTime[0]));
    codons[7].setSize(CodonConstants.SMALL);
    
    dummyStrings[0] = "GCT";
    dummyStrings[1] = "GCC";
    dummyStrings[2] = "GCA";
    dummyStrings[3] = "GCG";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[8] = new Codon(dummyStrings,"a");
    codons[8].addAminoAcidTiming(new AminoAcidTimeAttributes(tiny.convertNote(hydrophobic),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[8].setSize(CodonConstants.TINY);
    
    dummyStrings[0] = "TAT";
    dummyStrings[1] = "TAC";
    dummyStrings[2] = "";
    dummyStrings[3] = "";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[9] = new Codon(dummyStrings,"y");
    codons[9].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(aromatic),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[9].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(hydrophobic),playTime[2],durationTime[0],velocityTime[1],velocityTime[0]));
    codons[9].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(polar),playTime[4],durationTime[0],velocityTime[1],velocityTime[0]));
    codons[9].setSize(CodonConstants.LARGE);

    dummyStrings[0] = "TAA";
    dummyStrings[1] = "TAG";
    dummyStrings[2] = "TGA";
    dummyStrings[3] = "";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[10] = new Codon(dummyStrings,"z"); /*not an amino acid -> stop signal*/ 
                                              /*has to be inluded for incomplete sequences!*/
    codons[10].setSize(CodonConstants.SMALL);
    
    dummyStrings[0] = "CAT";
    dummyStrings[1] = "CAC";
    dummyStrings[2] = "";
    dummyStrings[3] = "";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[11] = new Codon(dummyStrings,"h");
    codons[11].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(positive),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[11].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(polar),playTime[2],durationTime[1],velocityTime[1],velocityTime[0]));
    codons[11].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(hydrophobic),playTime[3],durationTime[1],velocityTime[1],velocityTime[0]));
    codons[11].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(aromatic),playTime[4],durationTime[1],velocityTime[1],velocityTime[0]));
    codons[11].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(charged),playTime[5],durationTime[1],velocityTime[1],velocityTime[0]));
    codons[11].setSize(CodonConstants.LARGE);
    
    dummyStrings[0] = "CAA";
    dummyStrings[1] = "CAG";
    dummyStrings[2] = "";
    dummyStrings[3] = "";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[12] = new Codon(dummyStrings,"q");
    codons[12].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(polar),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[12].setSize(CodonConstants.LARGE);
    
    dummyStrings[0] = "AAT";
    dummyStrings[1] = "AAC";
    dummyStrings[2] = "";
    dummyStrings[3] = "";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[13] = new Codon(dummyStrings,"n");
    codons[13].addAminoAcidTiming(new AminoAcidTimeAttributes(small.convertNote(polar),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[13].setSize(CodonConstants.SMALL);
    
    dummyStrings[0] = "AAA";
    dummyStrings[1] = "AAG";
    dummyStrings[2] = "";
    dummyStrings[3] = "";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[14] = new Codon(dummyStrings,"k");
    codons[14].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(polar),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[14].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(hydrophobic),playTime[2],durationTime[0],velocityTime[1],velocityTime[0]));
    codons[14].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(charged),playTime[3],durationTime[1],velocityTime[1],velocityTime[0]));
    codons[14].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(positive),playTime[4],durationTime[1],velocityTime[1],velocityTime[0]));
    codons[14].setSize(CodonConstants.LARGE);
    
    dummyStrings[0] = "GAT";
    dummyStrings[1] = "GAC";
    dummyStrings[2] = "";
    dummyStrings[3] = "";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[15] = new Codon(dummyStrings,"d");
    codons[15].addAminoAcidTiming(new AminoAcidTimeAttributes(small.convertNote(polar),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[15].addAminoAcidTiming(new AminoAcidTimeAttributes(small.convertNote(charged),playTime[4],durationTime[0],velocityTime[1],velocityTime[0]));
    codons[15].setSize(CodonConstants.SMALL);
    
    dummyStrings[0] = "GAA";
    dummyStrings[1] = "GAG";
    dummyStrings[2] = "";
    dummyStrings[3] = "";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[16] = new Codon(dummyStrings,"e");
    codons[16].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(polar),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[16].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(charged),playTime[4],durationTime[0],velocityTime[1],velocityTime[0]));
    codons[16].setSize(CodonConstants.LARGE);
    
    dummyStrings[0] = "TGT";
    dummyStrings[1] = "TGC";
    dummyStrings[2] = "";
    dummyStrings[3] = "";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[17] = new Codon(dummyStrings,"c");
    codons[17].addAminoAcidTiming(new AminoAcidTimeAttributes(small.convertNote(hydrophobic),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[17].setSize(CodonConstants.SMALL);
    
    dummyStrings[0] = "TGG";
    dummyStrings[1] = "";
    dummyStrings[2] = "";
    dummyStrings[3] = "";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[18] = new Codon(dummyStrings,"w");
    codons[18].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(aromatic),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[18].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(hydrophobic),playTime[2],durationTime[0],velocityTime[1],velocityTime[0]));
    codons[18].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(polar),playTime[4],durationTime[0],velocityTime[1],velocityTime[0]));
    codons[18].setSize(CodonConstants.LARGE);
    
    dummyStrings[0] = "CGT";
    dummyStrings[1] = "CGC";
    dummyStrings[2] = "CGA";
    dummyStrings[3] = "CGG";
    dummyStrings[4] = "AGA";
    dummyStrings[5] = "AGG";
    codons[19] = new Codon(dummyStrings,"r");
    codons[19].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(polar),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[19].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(positive),playTime[2],durationTime[0],velocityTime[1],velocityTime[0]));
    codons[19].addAminoAcidTiming(new AminoAcidTimeAttributes(large.convertNote(charged),playTime[4],durationTime[0],velocityTime[1],velocityTime[0]));
    codons[19].setSize(CodonConstants.LARGE);
    
    dummyStrings[0] = "GGT";
    dummyStrings[1] = "GGC";
    dummyStrings[2] = "GGA";
    dummyStrings[3] = "GGG";
    dummyStrings[4] = "";
    dummyStrings[5] = "";
    codons[20] = new Codon(dummyStrings,"g");
    codons[20].addAminoAcidTiming(new AminoAcidTimeAttributes(tiny.convertNote(hydrophobic),playTime[0],durationTime[0],velocityTime[0],velocityTime[0]));
    codons[20].setSize(CodonConstants.TINY);

  }
  
  public void setNucMapping(String theNucleotide, MusicalNote theMappingNote)
  {
    if(theNucleotide.equalsIgnoreCase("a"))
    {
      nucA = theMappingNote;
    }
    else if(theNucleotide.equalsIgnoreCase("t"))
    {
      nucT = theMappingNote;
    }
    else if(theNucleotide.equalsIgnoreCase("g"))
    {
      nucG = theMappingNote;
    }
    else if(theNucleotide.equalsIgnoreCase("c"))
    {
      nucC = theMappingNote;
    }
  }  
  public MusicalNote getNucMapping(String theNucleotide)
  {
    MusicalNote returnMusicalNote = null;
    if(theNucleotide.equalsIgnoreCase("a"))
    {
      returnMusicalNote = nucA;
    }
    else if(theNucleotide.equalsIgnoreCase("t"))
    {
      returnMusicalNote = nucT;
    }
    else if(theNucleotide.equalsIgnoreCase("g"))
    {
      returnMusicalNote = nucG;
    }
    else if(theNucleotide.equalsIgnoreCase("c"))
    {
      returnMusicalNote = nucC;
    }
    else
    {
        System.out.println("The Nucleotide does not exist : "+theNucleotide);
    }
    return returnMusicalNote;
  }  
  public MusicalNote getNucMapping(String theNucleotide, String theCodingCodon)
  {
    MusicalNote dummyMusicalNote = this.getNucMapping(theNucleotide);
    MusicalNote returnMusicalNote = null;
    if(this.getCodonMappingFor(theCodingCodon).getSize()==CodonConstants.TINY)
    {
            returnMusicalNote = tiny.convertNote(dummyMusicalNote);
    }
    else if(this.getCodonMappingFor(theCodingCodon).getSize()==CodonConstants.SMALL)
    {
            returnMusicalNote = small.convertNote(dummyMusicalNote);
    }
    else if(this.getCodonMappingFor(theCodingCodon).getSize()==CodonConstants.LARGE)
    {
            returnMusicalNote = large.convertNote(dummyMusicalNote);
    }
    return returnMusicalNote;
  }  

  public Codon getCodonMappingFor(String theCodonString)
  {
    Codon returnCodon = null;
    
    for(int j = 0;j<CodonConstants.NUMBEROFAMINOACIDS;j++)
    {
      if(codons[j].isCoding(theCodonString))
      {
        returnCodon = codons[j];
        j = CodonConstants.NUMBEROFAMINOACIDS+1; /* found the right amin acid for this codon */
      }
    }
    return returnCodon;
  }
  
  // Called from the main class to shift all instances of sharp notes up or down, with 50/50 odds, to a whole note
  public void whole(boolean on){
      
      // initialise notes and indexes
      int[] nucs = {nucA.getIntegerRepresentation(), nucC.getIntegerRepresentation(), nucG.getIntegerRepresentation(), nucT.getIntegerRepresentation()};
      int[] aminos = {polar.getIntegerRepresentation(), hydrophobic.getIntegerRepresentation(), charged.getIntegerRepresentation(), positive.getIntegerRepresentation(), aliphatic.getIntegerRepresentation(), aromatic.getIntegerRepresentation()};
      int[] nucs_indexes = {nucANoteIndex, nucCNoteIndex, nucGNoteIndex, nucTNoteIndex};
      int[] aminos_indexes = {polarNoteIndex, hydroNoteIndex, chargedNoteIndex, positiveNoteIndex, aliphaticNoteIndex, aromaticNoteIndex};

      if (on && key == 0)
      {
        // loop through the array of nuc mappings
        int i = 0;
        while (i < nucs.length)
        {
            int j = 13;
            // loop through all possible sharp notes
            while (j <= 22) 
            {
                if (j == 17) j++; 
                // check the octaves
                if (nucs[i] == j ||nucs[i] == j+12 ||nucs[i] == j+24 || nucs[i] == j+36 || nucs[i] == j+48 || nucs[i] == j+60 || nucs[i] == j+72)
                {
                    // randomly map the nuc up or down to a whole note and updated indexes
                    double rnd = Math.random();
                    nucs[i] += (rnd >= 0.5) ? 1 : -1;
                    nucs_indexes[i] += (rnd >= 0.5) ? 1 : -1;
                    if (nucs_indexes[i] == 12) nucs_indexes[i] = 0;
                }
                j+=2;
            }
            i++;
       }
       int k = 0; 
       while (k < aminos.length)
       {
           int j = 13;
           while (j <= 22) 
            {
                if (j == 17) j++; 
                // check the octaves
                if (aminos[k] == j ||aminos[k] == j+12 ||aminos[k] == j+24 || aminos[k] == j+36 || aminos[k] == j+48 || aminos[k] == j+60 || aminos[k] == j+72)
                {
                    // randomly map the amino acid up or down to a whole note and updated indexes
                    double rnd = Math.random();
                    aminos[k] += (rnd >= 0.5) ? 1 : -1;
                    aminos_indexes[k] += (rnd >= 0.5) ? 1 : -1;
                    if (aminos_indexes[k] == 12) aminos_indexes[k] = 0;
                }
                j+=2;
                if (j == 17) j++;
            }
            k++;
       }
        // set new mappings and indexes
       nucA = new MusicalNote(nucs[0]); polar = new MusicalNote(aminos[0]);
       nucC = new MusicalNote(nucs[1]); hydrophobic = new MusicalNote(aminos[1]);
       nucG = new MusicalNote(nucs[2]); charged = new MusicalNote(aminos[2]);
       nucT = new MusicalNote(nucs[3]); positive = new MusicalNote(aminos[3]);
       nucANoteIndex = nucs_indexes[0]; aliphatic = new MusicalNote(aminos[4]);
       nucCNoteIndex = nucs_indexes[1]; aromatic = new MusicalNote(aminos[5]);
       nucGNoteIndex = nucs_indexes[2]; polarNoteIndex = aminos_indexes[0];              
       nucTNoteIndex = nucs_indexes[3]; hydroNoteIndex = aminos_indexes[1];
                                        chargedNoteIndex = aminos_indexes[2];
                                        positiveNoteIndex = aminos_indexes[3];
                                        aliphaticNoteIndex = aminos_indexes[4];
                                        aromaticNoteIndex = aminos_indexes[5];
     }
  }
    
  public int getGeneralCodonTime()
  {
      return generalCodonTime;
  }
  public void setGeneralCodonTime(int theGeneralCodonTime)
  {
      generalCodonTime = theGeneralCodonTime;
  }
  
  
}
  