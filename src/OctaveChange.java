/*
 * OctaveChange.java
 *
 * Created on 16 May 2000, 00:51
 */

/** 
 *
 * @author  aak97
 * @version 
 */
public class OctaveChange extends Object 
{
  private int octaveChange = 0;
  
  /** Creates new OctaveChange */
  public OctaveChange(int theOctaveChange)
  {
      octaveChange = theOctaveChange;
  }
  
  public int getOctaveChange()
  {
      return octaveChange;
  }
  
  public void setOctaveChage(int theOctaveChange)
  {
      octaveChange = theOctaveChange;
  }
  
  public MusicalNote convertNote(MusicalNote theNoteToBeChanged)
  {
      MusicalNote dummyMusicalNote = new MusicalNote(theNoteToBeChanged.getIntegerRepresentation()+(octaveChange*12));
      return dummyMusicalNote;
  }
  
  
  
}