/*
 * ConversionCore.java
 *
 * Created on 12 May 2000, 21:08
 */
 
import javax.sound.midi.*;
import java.io.*;
import java.lang.*;

/** 
 *
 * @author  aak97
 * @version 
 */
public class ConversionCore extends Object
{
  private String  inComingSequence  = "";
  private String  aminoAcidSequence = "";
  private Mapping mapping;
  public  Codon[] codonSequence;
  public int     nucSeqLength      = 0;

  private static  float		defaultTempoInBPM = 		(float) 120.0;
  private static  int		defaultTempoInPPQ = 		1500;
  private static  float 	defaultFactorPPQToBPM	=	(float) 12.5;

  private float   tempoInBPM        = defaultTempoInBPM;
  private int     tempoInPPQ        = defaultTempoInPPQ;
  
  /* MIDI stuff */
  
  private Track       topLineTrack;
  private Track       bassLineTrack;
  private Track[]     allTracks;
  private MidiEvent   topLineInstrumentMidi;
  private MidiEvent   bassLineInstrumentMidi;
  private MidiEvent   topLineReverbMidi;
  private MidiEvent   bassLineReverbMidi;
  private MidiEvent   topLinePitchMidi;
  private MidiEvent   bassLinePitchMidi;
  private MidiEvent   topLinePressureMidi;
  private MidiEvent   bassLinePressureMidi;
  private MidiEvent   topLineVolumeMidi;
  private MidiEvent   bassLineVolumeMidi;
  
  private Sequence    theSequence;
  
  
  private int     topLineInstrumentNumber = 0;
  private int     bassLineInstrumentNumber = 0;
  private int     topLineReverb            = 64;
  private int     bassLineReverb           = 64;
  private int     topLinePitch             = 64;
  private int     bassLinePitch            = 64;
  private int     topLinePressure          = 64;
  private int     bassLinePressure         = 64;
  private int     topLineVolume            = 128;
  private int     bassLineVolume           = 255;
 
  /** Creates new ConversionCore */
  public void ConversionCore()
  {
  }
  
  public void init()
  {
   
      mapping = new Mapping();
      mapping.init();
  }
  
  public void setNucSequence(String theNucSequence)
  {
      nucSeqLength = theNucSequence.length();
      inComingSequence = new String(theNucSequence.substring(0,((nucSeqLength/3)*3)));
      codonSequence = new Codon[inComingSequence.length()/3];
      aminoAcidSequence = new String();
  }
  public String getNucSequence()
  {
      return inComingSequence;
  }
  public String getAminoAcidSequence()
  {
      return aminoAcidSequence;
  }
  
  public int getNucSequenceLength()
  {
      return nucSeqLength;
  }
  
  public void setTopLineInstrumentNumber(int theInstrumentNumber)
  {
    topLineInstrumentNumber = theInstrumentNumber;
  }
  public int getTopLineInstrumentNumber()
  {
    return topLineInstrumentNumber;
  }

  public void setBassLineInstrumentNumber(int theInstrumentNumber)
  {
    bassLineInstrumentNumber = theInstrumentNumber;
  }
  public int getBassLineInstrumentNumber()
  {
    return bassLineInstrumentNumber;
  }
  public void setTopLinePressure(int thePressure)
  {
     topLinePressure = thePressure;
  }
  public int getTopLinePressure()
  {
     return topLinePressure;
  }
  public void setTopLinePitch(int thePitch)
  {
     topLinePitch = thePitch;
  }
  public int getTopLinePitch()
  {
     return topLinePitch;
  }
  public void setTopLineReverb(int theReverb)
  {
     topLineReverb = theReverb;
  }
  public int getTopLineReverb()
  {
     return topLineReverb;
  }
  public void setTopLineVolume(int theVolume)
  {
     topLineVolume = theVolume;
  }
  public int getTopLineVolume()
  {
     return topLineVolume;
  }
  public void setBassLinePressure(int thePressure)
  {
     bassLinePressure = thePressure;
  }
  public int getBassLinePressure()
  {
     return bassLinePressure;
  }
  public void setBassLinePitch(int thePitch)
  {
     bassLinePitch = thePitch;
  }
  public int getBassLinePitch()
  {
     return bassLinePitch;
  }
  public void setBassLineReverb(int theReverb)
  {
     bassLineReverb = theReverb;
  }
  public int getBassLineReverb()
  {
     return bassLineReverb;
  }
  public void setBassLineVolume(int theVolume)
  {
     bassLineVolume = theVolume;
  }
  public int getBassLineVolume()
  {
     return bassLineVolume;
  }


  public MidiEvent createShortMidiEvent(int theCommand, int theChannel, int theData1, int theData2, long theTime)
  {
      //System.out.println(""+theCommand+"\t"+theChannel+"\t"+theData1+"\t"+theData2+"\t"+theTime);
      int max = 255;
      ShortMessage dummyShortMessage;
      MidiEvent    dummyMidiEvent;
      int theData = theData1+theData2;
      int d2 = theData & max;
      int d1 = (theData-d2)/(max+1);
      d1 = theData1 / (max+1);
      d2 = theData2;

      //d2 = 15;
      try
      {
        dummyShortMessage = new ShortMessage();
	dummyShortMessage.setMessage(theCommand, theChannel, theData1, theData2);
        dummyMidiEvent    = new MidiEvent(dummyShortMessage,theTime);
      }
      catch (Exception e)
      {
          System.out.println(e);
          return null;
      }
      
      return dummyMidiEvent;
  }
  public MidiEvent createShortMidiEvent(int theCommand, int theChannel, int theData1, long theTime)
  {
      ShortMessage dummyShortMessage;
      MidiEvent    dummyMidiEvent;

      try
      {
        dummyShortMessage = new ShortMessage();
        dummyShortMessage.setMessage(theCommand, theChannel, theData1);
        dummyMidiEvent    = new MidiEvent(dummyShortMessage,theTime);
      }
      catch (Exception e)
      {
          System.out.println(e);
          return null;
      }

      return dummyMidiEvent;
  }
  public MidiEvent createMetaMidiEvent(int theType, String theData1, long theTime)
  {
      MetaMessage  dummyMetaMessage;
      MidiEvent    dummyMidiEvent;

      try
      {
        dummyMetaMessage = new MetaMessage();
        dummyMetaMessage.setMessage(theType, theData1.getBytes(), theData1.length());
        dummyMidiEvent    = new MidiEvent(dummyMetaMessage,theTime);
      }
      catch (Exception e)
      {
          System.out.println(e);
          return null;
      }
      
      return dummyMidiEvent;
  }
  
  public int getTempoInPPQ()
  {
      return tempoInPPQ;
  }
  
  public void setTempoInPPQ(int theTempoInPPQ)
  {
      tempoInPPQ = theTempoInPPQ;
      tempoInBPM = (float) tempoInPPQ/defaultFactorPPQToBPM;
  }
  public float getTempoInBPM()
  {
      return tempoInBPM;
  }

  public void setDefaultTempo()
  {
    tempoInBPM = defaultTempoInBPM;
    tempoInPPQ = defaultTempoInPPQ;
  }

  public void setTempoInBPM(float theTempoInBPM)
  {
    tempoInBPM = theTempoInBPM;
    tempoInPPQ = (int) (tempoInBPM*defaultFactorPPQToBPM);
  }

  
  public void changeInstruments()
  {
      Track                   instrumentTrack;       

      Track[]   allTracks = theSequence.getTracks();

      instrumentTrack = allTracks[2];
      instrumentTrack.remove(topLineInstrumentMidi);
      instrumentTrack.remove(bassLineInstrumentMidi);

      topLineInstrumentMidi  = this.createShortMidiEvent(ShortMessage.PROGRAM_CHANGE+2, 2, this.getTopLineInstrumentNumber(), 0, 1000);
      instrumentTrack.add(topLineInstrumentMidi);
      bassLineInstrumentMidi = this.createShortMidiEvent(ShortMessage.PROGRAM_CHANGE+1, 1, this.getBassLineInstrumentNumber(), 0, 1000);
      instrumentTrack.add(bassLineInstrumentMidi);
     
  }
  
  public void updateInstrumentSettings()
  {
      Track                   instrumentTrack;       

      Track[]   allTracks = theSequence.getTracks();

      instrumentTrack = allTracks[2];
      instrumentTrack.remove(topLineInstrumentMidi);
      instrumentTrack.remove(bassLineInstrumentMidi);

      topLineInstrumentMidi  = this.createShortMidiEvent(ShortMessage.PROGRAM_CHANGE+2, 2, this.getTopLineInstrumentNumber(), 0, 1000);
      instrumentTrack.add(topLineInstrumentMidi);
      topLineReverbMidi  = this.createShortMidiEvent(ShortMessage.PROGRAM_CHANGE+2, 2, this.getTopLineInstrumentNumber(), 0, 1000);
      instrumentTrack.add(topLineReverbMidi);
      
      topLinePressureMidi  = this.createShortMidiEvent(ShortMessage.CHANNEL_PRESSURE, 2, this.getTopLinePressure(), 0, 1000);
      instrumentTrack.add(topLinePressureMidi);
      topLinePitchMidi  = this.createShortMidiEvent(ShortMessage.PITCH_BEND, 2, this.getTopLinePitch(), 0, 1000);
      instrumentTrack.add(topLinePitchMidi);

      bassLineInstrumentMidi = this.createShortMidiEvent(ShortMessage.PROGRAM_CHANGE+1, 1, this.getBassLineInstrumentNumber(), 0, 1000);
      instrumentTrack.add(bassLineInstrumentMidi);
      bassLinePressureMidi  = this.createShortMidiEvent(ShortMessage.CHANNEL_PRESSURE, 2, this.getBassLinePressure(), 0, 1000);
      instrumentTrack.add(bassLinePressureMidi);
      bassLinePitchMidi  = this.createShortMidiEvent(ShortMessage.PITCH_BEND, 2, this.getBassLinePitch(), 0, 1000);
      instrumentTrack.add(bassLinePitchMidi);
     
  }

  public int getCodonTime()
  {
      return mapping.getGeneralCodonTime();
  }

  public int getNucTime()
  {
      return (int) (mapping.getGeneralCodonTime()/3);
  }
  
  public Sequence convertNucSequence(java.awt.Frame theParent)
  {
  
   
      MidiEvent               dummyMidiEvent;
      ShortMessage            dummyShortMessage;
      Sequence                dummySequence        = null;
      Track                   topLineTrack;
      Track                   bassLineTrack;       
      Track                   instrumentTrack;       
      
      MusicalNote             dummyMusicalNote1;
      MusicalNote             dummyMusicalNote2;
      MusicalNote             dummyMusicalNote3;
      MidiEvent               dummyMidiEvent1;
      MidiEvent               dummyMidiEvent2;
      MidiEvent               dummyMidiEvent3;
      ShortMessage            dummyShortMessage1   = null;
      ShortMessage            dummyShortMessage2   = null;
      ShortMessage            dummyShortMessage3   = null;
      
      int                     nucSeqLength         = 0;
      String                  dummyCodonString     = "";
      String                  dummyAminoAcidString = "";
      
      int                     codonTime            = mapping.getGeneralCodonTime();
      long                    startTime            = codonTime;
      long                    dummyTime            = startTime;
      int                     nucTime              = (int) codonTime/3;
      int                     nucTimeGap           = (int) new Double(nucTime*0.9).intValue();

      AminoAcidTimeAttributes aaTiming;
	

      if(!inComingSequence.equals(""))
      {
          try
          {
            dummySequence = new Sequence(Sequence.PPQ,defaultTempoInPPQ);
            theSequence = dummySequence;
          }
          catch(InvalidMidiDataException e)
          {
              System.out.println("O o "+e);
          }
          dummySequence.createTrack();  /*createtrack for topLine */
          dummySequence.createTrack();  /*createtrack for bassLine */
          dummySequence.createTrack();  /*createtrack DNA and Amino Acid sequences */

          /* store track in object variables */

          allTracks = dummySequence.getTracks();

          topLineTrack    = allTracks[0];
          bassLineTrack   = allTracks[1];
          instrumentTrack = allTracks[2];

          /* empty the aa -sequence */
          
          aminoAcidSequence = "";

          nucSeqLength = inComingSequence.length();
          
          if(nucSeqLength%3 != 0)
          {
              System.out.println("The Sequence length is "+nucSeqLength+" and divided by three is "+(nucSeqLength/3));
          }
          else
          {
              this.updateInstrumentSettings();
              for (int i = 0;i<inComingSequence.length()/3;i++)
              {
                  dummyCodonString = inComingSequence.substring(i*3,((i+1)*3));
                  codonSequence[i] = mapping.getCodonMappingFor(dummyCodonString);
                  
                  dummyMusicalNote1 = mapping.getNucMapping(inComingSequence.substring((i*3),(i*3)+1),dummyCodonString);
                  dummyMusicalNote2 = mapping.getNucMapping(inComingSequence.substring((i*3)+1,(i*3)+2),dummyCodonString);
                  dummyMusicalNote3 = mapping.getNucMapping(inComingSequence.substring((i*3)+2,(i*3)+3),dummyCodonString);
                  topLineTrack.add(this.createShortMidiEvent(ShortMessage.NOTE_ON, 2, dummyMusicalNote1.getIntegerRepresentation(), 100,dummyTime+(nucTime*0)));
                  topLineTrack.add(this.createShortMidiEvent(ShortMessage.NOTE_ON, 2, dummyMusicalNote2.getIntegerRepresentation(), 70,dummyTime+(nucTime*1)));
                  topLineTrack.add(this.createShortMidiEvent(ShortMessage.NOTE_ON, 2, dummyMusicalNote3.getIntegerRepresentation(), 70,dummyTime+(nucTime*2)));
                  topLineTrack.add(this.createShortMidiEvent(ShortMessage.NOTE_OFF, 2, dummyMusicalNote1.getIntegerRepresentation(), 100,dummyTime+(nucTime*0+nucTimeGap)));
                  topLineTrack.add(this.createShortMidiEvent(ShortMessage.NOTE_OFF, 2, dummyMusicalNote2.getIntegerRepresentation(), 100,dummyTime+(nucTime*1)+nucTimeGap));
                  topLineTrack.add(this.createShortMidiEvent(ShortMessage.NOTE_OFF, 2, dummyMusicalNote3.getIntegerRepresentation(), 100,dummyTime+(nucTime*2)+nucTimeGap));
                  
                  //adding the original sequence to the Midi sequence for the eventListener (i.e. display the Sequence while playing.
                  // leave out the amino acids ...
               
                  topLineTrack.add(this.createMetaMidiEvent(1,"",dummyTime+(nucTime*0)));
                  topLineTrack.add(this.createMetaMidiEvent(1,"",dummyTime+(nucTime*1)));
                  topLineTrack.add(this.createMetaMidiEvent(1,"",dummyTime+(nucTime*2)));


                  topLineTrack.add(this.createMetaMidiEvent(1,"",dummyTime+((nucTime/2)*1)));
                  topLineTrack.add(this.createMetaMidiEvent(1,"",dummyTime+((nucTime/2)*3)));
                  topLineTrack.add(this.createMetaMidiEvent(1,"",dummyTime+((nucTime/2)*5)));
                  // add the amino acid characteristics musical notes

                  for(int j = 0;j < codonSequence[i].getNumberOfAminoAcidTimings();j++)
                  {
                    aaTiming = codonSequence[i].getAminoAcidTiming(j);
                    bassLineTrack.add(this.createShortMidiEvent(ShortMessage.NOTE_ON, 1 ,aaTiming.getAttribute().getIntegerRepresentation(),aaTiming.getVelocityOnTime(),dummyTime+aaTiming.getPlayTime()));
                    bassLineTrack.add(this.createShortMidiEvent(ShortMessage.NOTE_OFF, 1, aaTiming.getAttribute().getIntegerRepresentation(),aaTiming.getVelocityOffTime(),dummyTime+aaTiming.getPlayTime()+aaTiming.getDurationTime()));
                  }
                  dummyTime = dummyTime + codonTime;

                  /* build up the amino acid sequence */

                  aminoAcidSequence = aminoAcidSequence + codonSequence[i].getAminoAcid();

	      }
              topLineTrack.add(this.createMetaMidiEvent(1,"next",dummyTime));
      	  }
          topLineTrack.add(this.createMetaMidiEvent(2,"end",dummyTime));
      }
      return dummySequence;

  }
 

  public static void main(String[] theArgs) throws Exception
  {

  }
}
