/*
 * CodonAttribute.java
 *
 * Created on 18 May 2000, 22:29
 */
 


/** 
 *
 * @author  aak97
 * @version 
 */
public class AminoAcidTimeAttributes extends Object {

  private MusicalNote   attribute         =   null;
  private long          playTime          =   0;
  private int           durationTime      =   0;
  private int           velocityOnTime    =   0;
  private int           velocityOffTime   =   0;
  
  /** Creates new CodonAttribute */
  public AminoAcidTimeAttributes() 
  {
  }
  public AminoAcidTimeAttributes(MusicalNote theAttribute, long thePlayTime,int theDurationTime, int theVelocityOnTime, int theVelocityOffTime)
  {
      attribute       = theAttribute;
      playTime        = thePlayTime;
      durationTime    = theDurationTime;
      velocityOnTime  = theVelocityOnTime;
      velocityOffTime = theVelocityOffTime;
  }
  
  public void setAttribute(MusicalNote theAttribute)
  {
      attribute = theAttribute;
  }
  public MusicalNote getAttribute()
  {
      return attribute;
  }
  public void setPlayTime(int thePlayTime)
  {
      playTime  = thePlayTime;
  }
  public long getPlayTime()
  {
      return playTime;
  }
  public void setDurationTime(int theDurationTime)
  {
      durationTime  = theDurationTime;
  }
  public int getDurationTime()
  {
      return durationTime;
  }
  public void setVelocityOnTime(int theVelocityOnTime)
  {
      velocityOnTime = theVelocityOnTime;
  }
  public int getVelocityOnTime()
  {
      return velocityOnTime;
  }
  public void setVelocityOffTime(int theVelocityOffTime)
  {
      velocityOffTime = theVelocityOffTime;
  }
  public int getVelocityOffTime()
  {
      return velocityOffTime;
  }
}