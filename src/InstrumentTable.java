/*
 * TestBean.java
 *
 * Created on 02 June 2000, 17:20
 */
 


import java.beans.*;
import java.awt.*;
import javax.swing.*;

import javax.swing.table.*;
import javax.swing.event.*;
import javax.sound.midi.*;

/** 
 *
 * @author  aak97
 * @version 
 */
public class InstrumentTable extends JPanel implements java.io.Serializable 
{
  private Synthesizer synthesizer;
  
  private Instrument instruments[];
  private Instrument instrument;

  private JTable table;

  private int row,col = 0;

  private String names[] =
  {
    "Piano", "Chromatic Perc.", "Organ", "Guitar",
    "Bass", "Strings", "Ensemble", "Brass",
    "Reed", "Pipe", "Synth Lead", "Synth Pad",
    "Synth Effects", "Ethnic", "Percussive", "Sound Effects"
  };
  private int nRows = 8;
  private int nCols = names.length; // just show 128 instruments


  /** Holds value of property instrumentNumber. */
  private int instrumentNumber;
  /** Creates new TestBean */
  public InstrumentTable() 
  {
    TableModel dataModel = new AbstractTableModel()
    {
      public int getColumnCount()
      {
        return nCols;
      }
      public int getRowCount()
      { 
        return nRows;
      }
      public Object getValueAt(int r, int c) 
      {
        if (instruments != null) 
        {
          return instruments[c*nRows+r].getName();
        } 
        else 
        {
          return Integer.toString(c*nRows+r);
        }
      }
      public String getColumnName(int c) 
      { 
        return names[c];
      }
      public Class getColumnClass(int c) 
      {
        return getValueAt(0, c).getClass();
      }
      public boolean isCellEditable(int r, int c)
      {
          return false;
      }
      public void setValueAt(Object obj, int r, int c) 
      {
      }
    };

    table = new JTable(dataModel);
    table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
           // Listener for row changes
    ListSelectionModel lsm = table.getSelectionModel();
    lsm.addListSelectionListener(
          new ListSelectionListener()
          {
            public void valueChanged(ListSelectionEvent e)
            {
            ListSelectionModel sm = (ListSelectionModel) e.getSource();
            if (!sm.isSelectionEmpty())
            {
              row = sm.getMinSelectionIndex();
            }
            programChange(col*nRows+row);
          }
        });
         // Listener for column changes
      lsm = table.getColumnModel().getSelectionModel();
      lsm.addListSelectionListener(
          new ListSelectionListener()
          {
            public void valueChanged(ListSelectionEvent e) 
            {
               ListSelectionModel sm = (ListSelectionModel) e.getSource();
               if (!sm.isSelectionEmpty()) 
               {
                col = sm.getMinSelectionIndex();
               }
               programChange(col*nRows+row);
            }
          });
      table.setPreferredScrollableViewportSize(new Dimension(nCols*550, 200));
      table.setCellSelectionEnabled(true);
      table.setColumnSelectionAllowed(true);
      for (int i = 0; i < names.length; i++)
      {
        TableColumn column = table.getColumn(names[i]);
        column.setPreferredWidth(110);
      }
      table.setAutoResizeMode(table.AUTO_RESIZE_OFF);

      JScrollPane sp = new JScrollPane(table);
      sp.setVerticalScrollBarPolicy(sp.VERTICAL_SCROLLBAR_NEVER);
      sp.setHorizontalScrollBarPolicy(sp.HORIZONTAL_SCROLLBAR_ALWAYS);
      add(sp);
  }

   public Dimension getPreferredSize() 
   {
      return new Dimension(names.length*110,170);
   }
   public Dimension getMaximumSize() 
   {
      return new Dimension(32000,180);
   }

   private void programChange(int program)
   {
      if (instruments != null)
      {
        instrument = instruments[program];
      }
      System.out.println(program);
   }

  /** Getter for property instrumentNumber.
   * @return Value of property instrumentNumber.
   */
  public int getInstrumentNumber() {
    return instrumentNumber;
  }
  /** Setter for property instrumentNumber.
   * @param instrumentNumber New value of property instrumentNumber.
   */
  public void setInstrumentNumber(int instrumentNumber) {
    this.instrumentNumber = instrumentNumber;
  }
  public void setSynthesizer(Synthesizer theSynthesizer) 
  {
      if(synthesizer != null)
      {
        synthesizer = theSynthesizer;
        instruments = synthesizer.getDefaultSoundbank().getInstruments();
//        synthesizer.loadInstrument(instruments[0]);
        repaint();
      }
  }
}