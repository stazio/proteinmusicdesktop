/*
 * MusicalNote.java
 *
 * Created on 15 May 2000, 19:58
 */

/** 
 *
 * @author  aak97, contributor - stas shvetsov
 * @version 
 */
public class MusicalNote extends Object 
{
  private String  stringRepresentation  = "";
  private int     integerRepresentation = 0;
  
  /** Creates new MusicalNote */
  public MusicalNote(String theStringRepresentation) 
  {
      stringRepresentation = new String (stringRepresentation);
      integerRepresentation = this.convertFromString(theStringRepresentation);
      //byteRepresentation = (integerRepresentation&&255);
  }
  
  public MusicalNote(int theIntegerRepresentation) 
  {
      integerRepresentation = theIntegerRepresentation;
      stringRepresentation =  this.convertFromInteger(theIntegerRepresentation);
  }
  
  public int getIntegerRepresentation()
  {
      return integerRepresentation;
  }
  
  public String getStringRepresentation()
  {
      return stringRepresentation;
  }
  public String convertFromInteger(int theIntegerRepresentation)
  {
      String  returnString       =   "";
      int     basicNote          =   0;
      int     ocataveMultiplier  =   0;
      
      ocataveMultiplier = (int) theIntegerRepresentation/12;
      basicNote         = (int) theIntegerRepresentation%12;
      
      /* string representation according to the basic node   */
      
      switch(basicNote)
      {
          case  0 : returnString = returnString + "C" ; break;
          case  1 : returnString = returnString + "c" ; break; // C Sharp
          case  2 : returnString = returnString + "D" ; break;
          case  3 : returnString = returnString + "d" ; break; // D Sharp
          case  4 : returnString = returnString + "E" ; break;
          case  5 : returnString = returnString + "F" ; break;
          case  6 : returnString = returnString + "f" ; break; // F Sharp
          case  7 : returnString = returnString + "G" ; break;
          case  8 : returnString = returnString + "g" ; break; // G Sharp
          case  9 : returnString = returnString + "A" ; break;
          case 10 : returnString = returnString + "a" ; break; // A Sharp
          case 11 : returnString = returnString + "B" ; break;
          default : returnString = returnString + this.getClass().getName() + " : could not convert into String!"  ; break;
      }

      /* convert int into String for getting the octave     */
      Integer dummyInteger = new Integer(ocataveMultiplier);
      returnString = returnString + dummyInteger.toString();
      return returnString;

    }
  
  public int convertFromString(String theStringRepresentation)
  {
      /* get the first character of the string representaion */
      /* for converting into musical note.                  */
      /* For example A3, first charater A                    */
    
      char dummyChar = theStringRepresentation.charAt(0); 
      
      /* convert into an integer                             */
    
      int  note          =   0;
      int  ocataveMultiplier  =   0;
      
      switch(dummyChar)
      {
          case 'C' : note =  0 ; break;
          case 'c' : note =  1 ; break; // C Sharp
          case 'D' : note =  2 ; break;
          case 'd' : note =  3 ; break; // D Sharp
          case 'E' : note =  4 ; break;
          case 'F' : note =  5 ; break;
          case 'f' : note =  6 ; break; // F Sharp
          case 'G' : note =  7 ; break;
          case 'g' : note =  8 ; break; // G Sharp
          case 'A' : note =  9 ; break;
          case 'a' : note = 10 ; break; // A Sharp
          case 'B' : note = 11 ; break;
          default : note = -1  ; break;
      }
     
      /* get the octave number (starts by zero)             */
      
      Integer dummyInteger = null;
      
     
      dummyInteger = new Integer(theStringRepresentation.substring(1));
      /* construct Integer from String                      */
      
      ocataveMultiplier = dummyInteger.intValue();
      
      /* return integer reprentation of the note            */
      
      return (ocataveMultiplier*12)+ note;
  }
  
  public static void main(String[] args)
  {
      String S = new String("A5");
     // int[] intArray = new int[128];
      MusicalNote[] MA = new MusicalNote[128];
      for(int i=0;i<=128;i++)
      {
                MA[i] = new MusicalNote(i);
                System.out.println("String representation of "+i+" is : "+ MA[i].getStringRepresentation());
      }
      MusicalNote M = new MusicalNote(S);
      System.out.println("String representation of "+S+" is : "+ M.getIntegerRepresentation());
      
      
  }

  
}
