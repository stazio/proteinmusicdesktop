/*
 * Instruments.java
 *
 * Created on 02 June 2000, 21:35
 */
 


import java.beans.*;
import javax.swing.*;

/** 
 *
 * @author  aak97
 * @version 
 */
public class Instruments extends JPanel implements java.io.Serializable {

  private static final String PROP_SAMPLE_PROPERTY = "SampleProperty"; 

  private String sampleProperty;

  private PropertyChangeSupport propertySupport;

  /** Creates new Instruments */
  public Instruments() {
    propertySupport = new PropertyChangeSupport ( this );
  }

  public String getSampleProperty () {
    return sampleProperty;
  }
  
  public void setSampleProperty (String value) {
    String oldValue = sampleProperty;
    sampleProperty = value;
    propertySupport.firePropertyChange (PROP_SAMPLE_PROPERTY, oldValue, sampleProperty);
  }


  public void addPropertyChangeListener (PropertyChangeListener listener) {
    propertySupport.addPropertyChangeListener (listener);
  }

  public void removePropertyChangeListener (PropertyChangeListener listener) {
    propertySupport.removePropertyChangeListener (listener);
  }

}