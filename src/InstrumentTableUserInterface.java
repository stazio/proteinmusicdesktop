/*
 * InstrumentTableUserInterface.java
 *
 * Created on 02 June 2000, 15:30
 */
 


/** 
 *
 * @author  aak97
 * @version 
 */
public interface InstrumentTableUserInterface 
{
  public void instrumentChange(int instrumentNumber);
}
