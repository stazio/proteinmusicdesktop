/*
 * CodonConstants.java
 *
 * Created on 17 May 2000, 20:45
 */
 
// test

/** 
 *
 * @author  aak97
 * @version 
 */
public interface CodonConstants 
{
  public static final int NUMBEROFAMINOACIDS = 21;

  public static final int LARGE = 2;
  public static final int SMALL = 1;
  public static final int TINY  = 0;
  
  public static final int ALIPHATIC   = 0;
  public static final int AROMATIC    = 1;
  public static final int CHARGED     = 2;
  public static final int HYDROPHOBIC = 3;
  public static final int POLAR       = 4;
  public static final int POSITIVE    = 5;
  
}
