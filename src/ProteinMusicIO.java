/*
 * ProteinMusicIO.java
 *
 * Created on 21 May 2000, 21:50
 */
 
import java.io.*;

/** 
 *
 * @author  aak97
 * @version 
 */
public class ProteinMusicIO extends Object {

  BufferedReader inDnaReader = null;
  /** Creates new ProteinMusicIO */
  public ProteinMusicIO() 
  {
      inDnaReader = null;
  }
  
  /*
  public String getDnaSequenceFromFile(String theFileName)
  {
    String returnString   = "";
    String decisionString = "";
    try
    {
      inDnaReader = new BufferedReader(new FileReader(theFileName));
    }
    catch(Exception e)
    {
        System.out.println(""+this.getClass()+"\n"+e);
    }
    try
    {
      decisionString = inDnaReader.readLine();
    }
    catch(Exception e)
    {
        System.out.println(""+this.getClass()+"\n"+e);
    }
    if((decisionString !=null)&&(decisionString.length()>0)&&(decisionString.charAt(0) == '>')) 
    {
      try
      {
        returnString = this.getDnaSequenceFromFastaFile();
      }
      catch(Exception e)
      {
        System.out.println(""+this.getClass()+"\n"+e);
      }
    }
    else
    {
    }
    
    return returnString;
  }
*/
  public String getSequenceFromFile(String theFileName)
  {
    String fileString     = "";
    try
    {
      inDnaReader = new BufferedReader(new FileReader(theFileName));
      fileString  = this.getAsciiFile();
    }
    catch(Exception e)
    {
        System.out.println(""+this.getClass()+"\n"+e);
    }
    return this.getSequenceFromString(fileString);

  }

  public String getSequenceFromString(String seqString)
  {
    String returnString   = "";
    if(testForFasta(seqString))
    {
        returnString = convertSequenceFromFastaString(seqString);
    }
    if(testForEmbl(seqString))
    {
        returnString = convertSequenceFromEmblString(seqString);
    }
    if(testForNcbi(seqString))
    {
        returnString = convertSequenceFromNcbiString(seqString);
    }
    return returnString;
  }
  /* 
  private String getDnaSequenceFromFastaFile() throws java.io.IOException
  {
      String  dummyReturnString = "";
      String  dummyReadInString = "";
      String  dummyString       = "";
      boolean finished          = false;
      
      do
      {
          dummyReadInString = inDnaReader.readLine();
          if(dummyReadInString.equals(""))
          {
              finished = true;
          }
          else
          {
            //System.out.println(dummyReadInString);
            for(int i = 0;i<dummyReadInString.length();i++)
            {
                dummyString = dummyReadInString.substring(i,i+1);
                if(dummyString.equalsIgnoreCase("a")||
                    dummyString.equalsIgnoreCase("t")||
                    dummyString.equalsIgnoreCase("g")||
                    dummyString.equalsIgnoreCase("c"))
                {
                    dummyReturnString = dummyReturnString + dummyString;
                }
            }
          }
          
      }
      while(!finished);
      
      return dummyReturnString;
      
  }
  */
  private String getAsciiFile()  throws java.io.IOException
  {
      String  dummyReturnString = "";
      String  dummyReadInString = "";
      String  dummyString       = "";
      boolean finished          = false;
      
      do
      {
          dummyReadInString = inDnaReader.readLine();
          if(dummyReadInString == null)
          {
              finished = true;
          }
          else
          {
              dummyReturnString = dummyReturnString + dummyReadInString+"\n";
          }
          
      }
      while(!finished);
      
      return dummyReturnString;
  }
  
  private boolean testForFasta(String theFileString)
  { 
    if((theFileString !=null)&&(theFileString.length()>0)&&(theFileString.charAt(0) == '>')) /* FASTA format */
    {
        return true;
    }    
    else
    {
        return false;
    }
  }
  public  String convertSequenceFromFastaString(String theFASTASequence)
  {
    String  dummyReturnString = "";
    String  dummyString       = "";
    String  dummyReadInString = "";
    try
    {
      dummyReadInString = theFASTASequence.substring(theFASTASequence.indexOf("\n"),theFASTASequence.length());
    }
    catch(Exception e)
    {
        System.out.println("ProteinMusicIO:\t Problems with FASTA sequence");
        return null;
    }
    
    for(int i = 0;i<dummyReadInString.length();i++)
    {
      dummyString = dummyReadInString.substring(i,i+1);
      if(dummyString.equalsIgnoreCase("a")||
         dummyString.equalsIgnoreCase("t")||
         dummyString.equalsIgnoreCase("g")||
         dummyString.equalsIgnoreCase("c"))
         {
            dummyReturnString = dummyReturnString + dummyString;
         }
    }
    return dummyReturnString;

  }

  private boolean testForEmbl(String theFileString)
  { 
    if((theFileString !=null)&&(theFileString.length()>0)&&(theFileString.indexOf("SQ   Sequence")>0)&&(theFileString.indexOf("//")>0)) /* EMBL format */
    {
        return true;
    }    
    else
    {
        return false;
    }
  }
  private boolean testForNcbi(String theFileString)
  { 
    if((theFileString !=null)&&(theFileString.length()>0)&&(theFileString.indexOf("BASE COUNT")>0)&&(theFileString.indexOf("//")>0)) /* NCBI format */
    {
        return true;
    }    
    else
    {
        return false;
    }
  }
  
    public  String convertSequenceFromEmblString(String theEmblSequence)
    {
      String  dummyReturnString  = "";
      String  dummyString        = "";
      String  dummyReadInString1 = "";
      String  dummyReadInString2 = "";
      try
      {
        dummyReadInString1 = theEmblSequence.substring(theEmblSequence.indexOf("SQ   Sequence"),(theEmblSequence.indexOf("//")-1));
        dummyReadInString2 = dummyReadInString1.substring(dummyReadInString1.indexOf("\n"),dummyReadInString1.length());
      }
      catch(Exception e)
      {
        System.out.println("ProteinMusicIO:\t Problems with FASTA sequence");
        return null;
      }
    
      for(int i = 0;i<dummyReadInString2.length();i++)
      {
        dummyString = dummyReadInString2.substring(i,i+1);
        if( dummyString.equalsIgnoreCase("a")||
            dummyString.equalsIgnoreCase("t")||
            dummyString.equalsIgnoreCase("g")||
            dummyString.equalsIgnoreCase("c"))
        {
              dummyReturnString = dummyReturnString + dummyString;
        }
    }
    return dummyReturnString;

  }
    public  String convertSequenceFromNcbiString(String theNcbiSequence)
    {
      String  dummyReturnString  = "";
      String  dummyString        = "";
      String  dummyReadInString1 = "";
      String  dummyReadInString2 = "";
      try
      {
        dummyReadInString1 = theNcbiSequence.substring(theNcbiSequence.indexOf("ORIGIN"),(theNcbiSequence.lastIndexOf("//")-1));
        dummyReadInString2 = dummyReadInString1.substring(dummyReadInString1.indexOf("\n"),dummyReadInString1.length());
      }
      catch(Exception e)
      {
        System.out.println("ProteinMusicIO:\t Problems with FASTA sequence");
        return null;
      }
    
      for(int i = 0;i<dummyReadInString2.length();i++)
      {
        dummyString = dummyReadInString2.substring(i,i+1);
        if( dummyString.equalsIgnoreCase("a")||
            dummyString.equalsIgnoreCase("t")||
            dummyString.equalsIgnoreCase("g")||
            dummyString.equalsIgnoreCase("c"))
        {
              dummyReturnString = dummyReturnString + dummyString;
        }
    }
    return dummyReturnString;

  }

  
  public static void main(String [] theArgs)
  {
      ProteinMusicIO PIO = new ProteinMusicIO();
      System.out.println(PIO.getSequenceFromFile("GENE.AA"));
  }
  
}