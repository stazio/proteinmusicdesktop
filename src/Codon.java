/*
 * Codon.java
 *
 * Created on 17 May 2000, 01:03
 */
 
import java.util.*;
import java.lang.*;
/** 
 *
 * @author  aak97
 * @version 
 */
public class Codon extends Object implements CodonConstants
{
  
  public String[] codingCodons = new String[6];
  public String   aminoAcid    = "";
  private int     size         = -1;  
  
  private Vector  aminoAcidTimings;
  
  
  /** Creates new Codon */
  public Codon() 
  {
      aminoAcidTimings = new Vector();
  }

  public Codon(String[] theCodons,String theAminoAcid)
  {
      this.setCodons(theCodons);
      this.setAminoAcid(theAminoAcid);
      aminoAcidTimings = new Vector();
  }
 
  public void setCodons(String[] theCodons)
  {
      for(int i = 0;i<theCodons.length;i++)
      {
          codingCodons[i] = new String(theCodons[i]);
      }
  }
  
  public String[] getCodons()
  {
      return codingCodons;
  }
  
  public void setAminoAcid(String theAminoAcid)
  {
      aminoAcid =  new String(theAminoAcid);
  }

  public String getAminoAcid()
  {
      return aminoAcid;
  }
  
  public void addAminoAcidTiming(AminoAcidTimeAttributes theAminoAcidTiming)
  {
    aminoAcidTimings.add(theAminoAcidTiming);
  }
  
  public AminoAcidTimeAttributes getAminoAcidTiming(int indexOfAminoAcidTiming)
  {
    AminoAcidTimeAttributes dummyAminoAcidTimeAttributes;
    try
    {
        dummyAminoAcidTimeAttributes = (AminoAcidTimeAttributes) aminoAcidTimings.get(indexOfAminoAcidTiming);
    }
    catch(ArrayIndexOutOfBoundsException e)
    {
        dummyAminoAcidTimeAttributes = null;
    }
    
    return dummyAminoAcidTimeAttributes;
  }
  
  public int getNumberOfAminoAcidTimings()
  {
      return aminoAcidTimings.size();
  }
  
  public void setSize(int theSize)
  {
      if((theSize >= CodonConstants.TINY)||(theSize <= CodonConstants.LARGE ))
      {
          size = theSize;
      }
  }

  public int getSize()
  {
      return size;
  }
  
  public boolean isCoding(String theCodingTestString)
  {
      for(int i=0;i<codingCodons.length;i++)
      {
          if(codingCodons[i].equalsIgnoreCase(theCodingTestString))
          {
              return true;
          }
      }
      return false;
  }
  
  public String toString()
  {
      String returnString = "";
      
      returnString = returnString + this.getAminoAcid() + "\t";
      for(int i =0;i<this.codingCodons.length;i++)
      {
          if(!(this.codingCodons[i].equals("")))
          {
            returnString = returnString + this.codingCodons + "\t";
          }
      }
      returnString = returnString + "\n";
      return returnString;
  }
  
  public static void main(String[] theArgs)
  {
      String[] codon = new String[2];
      codon[0] = "TTT";
      codon[1] = "GGG";
      Codon C = new Codon(codon,"p");
      System.out.println(C.getAminoAcid());
  }
    
}