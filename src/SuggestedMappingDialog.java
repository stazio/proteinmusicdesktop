import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;

import javax.swing.UIManager.*;



public class SuggestedMappingDialog extends javax.swing.JDialog { 

  private int     theButtonPressed = -1;
  private boolean buttonPressed = false;
  private Frame parent;
  Mapping mapping = new Mapping();

  /** Creates new form CustomMappingDialog */
  public SuggestedMappingDialog(java.awt.Frame theParent, boolean modal) {
    super (theParent, modal);
    parent = theParent;
    initLookAndFeel(); 
    initComponents ();
    pack ();
  }
  
  // Initialise Nimbus Look and Feel
  private static void initLookAndFeel() 
   {
       try 
       {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) 
            {
                if ("Nimbus".equals(info.getName())) 
                {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
       }
       catch (Exception e) {}
  }
  
  // Initialise components and their corresponding handlers
  public void initComponents () 
  {
    jPanel2 = new javax.swing.JPanel ();
    jSeparator1 = new javax.swing.JSeparator ();
    journey_mapping_B = new javax.swing.JButton ();
    contemporary_mapping_B = new javax.swing.JButton ();
    A_flat_major_6_mapping_B = new javax.swing.JButton();
    Diminished_D_mapping_B = new javax.swing.JButton();
    bass_ostinato_mapping_B = new javax.swing.JButton();
    raindrops_mapping_B = new javax.swing.JButton();
    resetButton = new javax.swing.JButton ();
    cancelButton = new javax.swing.JButton ();
    save_and_close_Button = new javax.swing.JButton();
    jPanel1 = new javax.swing.JPanel ();
    // jScrollPane1 = new javax.swing.JScrollPane ();
  
    setTitle ("Suggested mappings for this program");
    setResizable (false);
    addWindowListener (new java.awt.event.WindowAdapter () {
      public void windowClosing (java.awt.event.WindowEvent evt) {
        closeDialog (evt);
      }
    }
    );
    

    jPanel2.setLayout (new java.awt.GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    
      journey_mapping_B.setPreferredSize(new Dimension(80, 80));
      journey_mapping_B.setText("Journey");
      c.fill = GridBagConstraints.HORIZONTAL;
      c.insets = new Insets(10, 10, 10, 10);
      c.gridx = 0;
      c.gridy = 0;
      journey_mapping_B.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          journeyButtonPerformed (evt);
        }
      }
      );
  
      jPanel2.add (journey_mapping_B, c);
      
      contemporary_mapping_B.setPreferredSize(new Dimension(80, 80));
      contemporary_mapping_B.setText("Contemporary");
      c.fill = GridBagConstraints.HORIZONTAL;
      c.gridx = 1;
      c.gridy = 0;
      contemporary_mapping_B.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          contemporaryButtonPerformed (evt);
        }
      }
      );
  
      jPanel2.add (contemporary_mapping_B, c);
      
      A_flat_major_6_mapping_B.setPreferredSize(new Dimension(80, 80));
      A_flat_major_6_mapping_B.setText("A Flat Major 6");
      c.fill = GridBagConstraints.HORIZONTAL;
      c.gridx = 2;
      c.gridy = 0;
       A_flat_major_6_mapping_B.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          A_flat_major_6_ButtonPerformed (evt);
        }
      }
      );
  
      jPanel2.add (A_flat_major_6_mapping_B, c);
      
      Diminished_D_mapping_B.setPreferredSize(new Dimension(80, 80));
      Diminished_D_mapping_B.setText("Diminished D");
      c.fill = GridBagConstraints.HORIZONTAL;
      // c.insets = new Insets(0, 20, 20, 0);
      c.gridx = 0;
      c.gridy = 1;
       Diminished_D_mapping_B.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          Diminished_D_ButtonPerformed (evt);
        }
      }
      );
  
      jPanel2.add (Diminished_D_mapping_B, c);
      
      bass_ostinato_mapping_B.setPreferredSize(new Dimension(80, 80));
      bass_ostinato_mapping_B.setText("Bass Ostinato");
      c.fill = GridBagConstraints.HORIZONTAL;
      c.gridx = 1;
      c.gridy = 1;
       bass_ostinato_mapping_B.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          bass_ostinato_ButtonPerformed (evt);
        }
      }
      );
  
      jPanel2.add (bass_ostinato_mapping_B, c);
    
      raindrops_mapping_B.setPreferredSize(new Dimension(80, 80));
      raindrops_mapping_B.setText("Raindrops");
      c.fill = GridBagConstraints.HORIZONTAL;
      // c.insets = new Insets(0, 20, 20, 20);
      c.gridx = 2;
      c.gridy = 1;
       raindrops_mapping_B.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          raindrops_ButtonPerformed (evt);
        }
      }
      );
  
      jPanel2.add (raindrops_mapping_B, c);
      
      save_and_close_Button.setPreferredSize(new Dimension(120, 35));
      save_and_close_Button.setText ("Save & Close");
      c.fill = GridBagConstraints.HORIZONTAL;
      // c.insets = new Insets(30,0,0,0);
      c.gridx = 0;
      c.gridy = 3;
      save_and_close_Button.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          save_and_close_ButtonPerformed (evt);
        }
      }
      );
  
      jPanel2.add (save_and_close_Button, c);
      
      resetButton.setPreferredSize(new Dimension(120, 35));
      resetButton.setText ("Reset");
      c.fill = GridBagConstraints.HORIZONTAL;
      c.gridx = 1;
      c.gridy = 3;
      resetButton.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          resetButtonPerformed (evt);
        }
      }
      );
  
      jPanel2.add (resetButton, c);
      
      cancelButton.setPreferredSize(new Dimension(120, 35));
      cancelButton.setText ("Cancel");
      c.fill = GridBagConstraints.HORIZONTAL;
      // c.insets = new Insets(0, 20, 20, 20);
      c.gridx = 2;
      c.gridy = 3;
      cancelButton.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          cancelButtonPerformed (evt);
        }
      }
      );
  
      jPanel2.add (cancelButton, c);
    
      jPanel1.setBorder(new javax.swing.border.TitledBorder("Please select the new mappings for the top line and bass line"));
      //jScrollPane1.setPreferredSize (new java.awt.Dimension(300, 150));
      //jPanel1.add (jScrollPane1);
      
      getContentPane ().add (jPanel1, java.awt.BorderLayout.NORTH);
      getContentPane().add(jPanel2, java.awt.BorderLayout.SOUTH);

  }
  
  // Set the suggested mapping index for the mapping class, indexes for the custom mapping dialog and change the
  private void journeyButtonPerformed (java.awt.event.ActionEvent evt) {
    mapping.suggestedMappingIndex = 1;
    mapping.nucANoteIndex = 10; mapping.nucAOctaveIndex = 3;
    mapping.nucTNoteIndex = 3; mapping.nucCOctaveIndex = 3;
    mapping.nucGNoteIndex = 7; mapping.nucGOctaveIndex = 3;
    mapping.nucCNoteIndex = 10; mapping.nucTOctaveIndex = 3;
    
    mapping.polarNoteIndex = 3; mapping.polarOctaveIndex = 5;
    mapping.hydroNoteIndex = 8; mapping.hydroOctaveIndex = 5;
    mapping.chargedNoteIndex = 5; mapping.chargedOctaveIndex = 5;
    mapping.positiveNoteIndex = 7; mapping.positiveOctaveIndex = 5;
    mapping.aliphaticNoteIndex = 0; mapping.aliphaticOctaveIndex = 5;
    mapping.aromaticNoteIndex = 10; mapping.aromaticOctaveIndex = 5;
  
    journey_mapping_B.setBackground(Color.GRAY);
    journey_mapping_B.setForeground(Color.BLACK);
    
    contemporary_mapping_B.setBackground(Color.LIGHT_GRAY);
    A_flat_major_6_mapping_B.setBackground(Color.LIGHT_GRAY);
    Diminished_D_mapping_B.setBackground(Color.LIGHT_GRAY);
    bass_ostinato_mapping_B.setBackground(Color.LIGHT_GRAY);
    raindrops_mapping_B.setBackground(Color.LIGHT_GRAY);
  }
  
  private void contemporaryButtonPerformed (java.awt.event.ActionEvent evt) {
    mapping.suggestedMappingIndex = 2;
    mapping.nucANoteIndex = 3; mapping.nucAOctaveIndex = 5;
    mapping.nucTNoteIndex = 10; mapping.nucCOctaveIndex = 5;
    mapping.nucGNoteIndex = 3; mapping.nucGOctaveIndex = 6;
    mapping.nucCNoteIndex = 7; mapping.nucTOctaveIndex = 5;
    
    mapping.polarNoteIndex = 1; mapping.polarOctaveIndex = 3;
    mapping.hydroNoteIndex = 6; mapping.hydroOctaveIndex = 3;
    mapping.chargedNoteIndex = 4; mapping.chargedOctaveIndex = 3;
    mapping.positiveNoteIndex = 9; mapping.positiveOctaveIndex = 3;
    mapping.aliphaticNoteIndex = 11; mapping.aliphaticOctaveIndex = 4;
    mapping.aromaticNoteIndex = 2; mapping.aromaticOctaveIndex = 3;
    contemporary_mapping_B.setBackground(Color.GRAY);
    contemporary_mapping_B.setForeground(Color.BLACK);
    
    journey_mapping_B.setBackground(Color.LIGHT_GRAY);
    A_flat_major_6_mapping_B.setBackground(Color.LIGHT_GRAY);
    Diminished_D_mapping_B.setBackground(Color.LIGHT_GRAY);
    bass_ostinato_mapping_B.setBackground(Color.LIGHT_GRAY);
    raindrops_mapping_B.setBackground(Color.LIGHT_GRAY);
  }
  
  private void A_flat_major_6_ButtonPerformed (java.awt.event.ActionEvent evt) {
    mapping.suggestedMappingIndex = 3;
    mapping.nucANoteIndex = 6; mapping.nucAOctaveIndex = 5;
    mapping.nucTNoteIndex = 8; mapping.nucCOctaveIndex = 5;
    mapping.nucGNoteIndex = 10; mapping.nucGOctaveIndex = 5;
    mapping.nucCNoteIndex = 11; mapping.nucTOctaveIndex = 5;
    
    mapping.polarNoteIndex = 6; mapping.polarOctaveIndex = 4;
    mapping.hydroNoteIndex = 11; mapping.hydroOctaveIndex = 4;
    mapping.chargedNoteIndex = 3; mapping.chargedOctaveIndex = 5;
    mapping.positiveNoteIndex = 8; mapping.positiveOctaveIndex = 5;
    mapping.aliphaticNoteIndex = 11; mapping.aliphaticOctaveIndex = 6;
    mapping.aromaticNoteIndex = 3; mapping.aromaticOctaveIndex = 6;
    A_flat_major_6_mapping_B.setBackground(Color.GRAY);
    A_flat_major_6_mapping_B.setForeground(Color.BLACK);
    
    journey_mapping_B.setBackground(Color.LIGHT_GRAY);
    contemporary_mapping_B.setBackground(Color.LIGHT_GRAY);
    Diminished_D_mapping_B.setBackground(Color.LIGHT_GRAY);
    bass_ostinato_mapping_B.setBackground(Color.LIGHT_GRAY);
    raindrops_mapping_B.setBackground(Color.LIGHT_GRAY);
   
  }
  
  private void Diminished_D_ButtonPerformed (java.awt.event.ActionEvent evt) {
    mapping.suggestedMappingIndex = 4;
    mapping.nucANoteIndex = 2; mapping.nucAOctaveIndex = 5;
    mapping.nucTNoteIndex = 5; mapping.nucAOctaveIndex = 6;
    mapping.nucGNoteIndex = 8; mapping.nucAOctaveIndex = 6;
    mapping.nucCNoteIndex = 11; mapping.nucAOctaveIndex = 6;
    
    mapping.polarNoteIndex = 5; mapping.polarOctaveIndex = 4;
    mapping.hydroNoteIndex = 8; mapping.hydroOctaveIndex = 4;
    mapping.chargedNoteIndex = 11; mapping.chargedOctaveIndex = 4;
    mapping.positiveNoteIndex = 2; mapping.positiveOctaveIndex = 4;
    mapping.aliphaticNoteIndex = 5; mapping.aliphaticOctaveIndex = 5;
    mapping.aromaticNoteIndex = 8; mapping.aromaticOctaveIndex = 5;
    Diminished_D_mapping_B.setBackground(Color.GRAY);
    Diminished_D_mapping_B.setForeground(Color.BLACK);
    
    journey_mapping_B.setBackground(Color.LIGHT_GRAY);
    contemporary_mapping_B.setBackground(Color.LIGHT_GRAY);
    A_flat_major_6_mapping_B.setBackground(Color.LIGHT_GRAY);
    bass_ostinato_mapping_B.setBackground(Color.LIGHT_GRAY);
    raindrops_mapping_B.setBackground(Color.LIGHT_GRAY);
  }
  
  private void bass_ostinato_ButtonPerformed (java.awt.event.ActionEvent evt) {
    mapping.suggestedMappingIndex = 5;
    mapping.nucANoteIndex = 5; mapping.nucAOctaveIndex = 5;
    mapping.nucTNoteIndex = 0; mapping.nucAOctaveIndex = 5;
    mapping.nucGNoteIndex = 8; mapping.nucAOctaveIndex = 5;
    mapping.nucCNoteIndex = 3; mapping.nucAOctaveIndex = 5;
    
    mapping.polarNoteIndex = 10; mapping.polarOctaveIndex = 3;
    mapping.hydroNoteIndex = 3; mapping.hydroOctaveIndex = 3;
    mapping.chargedNoteIndex = 8; mapping.chargedOctaveIndex = 3;
    mapping.positiveNoteIndex = 5; mapping.positiveOctaveIndex = 4;
    mapping.aliphaticNoteIndex = 8; mapping.aliphaticOctaveIndex = 2;
    mapping.aromaticNoteIndex = 8; mapping.aromaticOctaveIndex = 3;
    bass_ostinato_mapping_B.setBackground(Color.GRAY);
    bass_ostinato_mapping_B.setForeground(Color.BLACK);
    
    journey_mapping_B.setBackground(Color.LIGHT_GRAY);
    contemporary_mapping_B.setBackground(Color.LIGHT_GRAY);
    A_flat_major_6_mapping_B.setBackground(Color.LIGHT_GRAY);
    Diminished_D_mapping_B.setBackground(Color.LIGHT_GRAY);
    raindrops_mapping_B.setBackground(Color.LIGHT_GRAY);
  }
  
  private void raindrops_ButtonPerformed (java.awt.event.ActionEvent evt) {
    mapping.suggestedMappingIndex = 6;
    mapping.nucANoteIndex = 6; mapping.nucAOctaveIndex = 5;
    mapping.nucTNoteIndex = 9; mapping.nucAOctaveIndex = 5;
    mapping.nucGNoteIndex = 1; mapping.nucAOctaveIndex = 6;
    mapping.nucCNoteIndex = 6; mapping.nucAOctaveIndex = 6;
    
    mapping.polarNoteIndex = 6; mapping.polarOctaveIndex = 5;
    mapping.hydroNoteIndex = 9; mapping.hydroOctaveIndex = 5;
    mapping.chargedNoteIndex = 11; mapping.chargedOctaveIndex = 5;
    mapping.positiveNoteIndex = 1; mapping.positiveOctaveIndex = 5;
    mapping.aliphaticNoteIndex = 4; mapping.aliphaticOctaveIndex = 6;
    mapping.aromaticNoteIndex = 6; mapping.aromaticOctaveIndex = 6;
    raindrops_mapping_B.setBackground(Color.GRAY);
    raindrops_mapping_B.setForeground(Color.BLACK);
    
    journey_mapping_B.setBackground(Color.LIGHT_GRAY);
    contemporary_mapping_B.setBackground(Color.LIGHT_GRAY);
    A_flat_major_6_mapping_B.setBackground(Color.LIGHT_GRAY);
    Diminished_D_mapping_B.setBackground(Color.LIGHT_GRAY);
    bass_ostinato_mapping_B.setBackground(Color.LIGHT_GRAY);
  }
  
  private void save_and_close_ButtonPerformed (java.awt.event.ActionEvent evt) {
    raindrops_mapping_B.setBackground(Color.LIGHT_GRAY);
    journey_mapping_B.setBackground(Color.LIGHT_GRAY);
    contemporary_mapping_B.setBackground(Color.LIGHT_GRAY);
    A_flat_major_6_mapping_B.setBackground(Color.LIGHT_GRAY);
    Diminished_D_mapping_B.setBackground(Color.LIGHT_GRAY);
    bass_ostinato_mapping_B.setBackground(Color.LIGHT_GRAY);
      
    theButtonPressed = 1;
    buttonPressed = true;
    mapping.key = 0;  
    setVisible(false);
  }
  
    private void cancelButtonPerformed (java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonPerformed
    // Add your handling code here:
    theButtonPressed = 0;
    setVisible(false);
  }
    
    private void resetButtonPerformed (java.awt.event.ActionEvent evt) {
    resetAll();
    theButtonPressed = 1;
    buttonPressed = true;
  }
    
  
    /** Closes the dialog */
  private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
    setVisible (false);
    dispose ();
  }//GEN-LAST:event_closeDialog
  
  // Reset back to default indexes + resets musical score and sets all button colours back to default
  public void resetAll()
  {
      mapping.suggestedMappingIndex = 0;
      mapping.key = 1;
      mapping.majorOrMinor = 0;
      
      mapping.nucANoteIndex = 0;
      mapping.nucAOctaveIndex = 5;
      mapping.nucCNoteIndex = 10;
      mapping.nucCOctaveIndex = 5;
      mapping.nucGNoteIndex = 3;
      mapping.nucGOctaveIndex = 5;
      mapping.nucTNoteIndex = 7;
      mapping.nucTOctaveIndex = 5;
  
      mapping.polarNoteIndex = 0;
      mapping.polarOctaveIndex = 3;
      mapping.hydroNoteIndex = 3;
      mapping.hydroOctaveIndex = 3;
      mapping.chargedNoteIndex = 8;
      mapping.chargedOctaveIndex = 3;
      mapping.positiveNoteIndex = 7;
      mapping.positiveOctaveIndex = 3;
      mapping.aliphaticNoteIndex = 10;
      mapping.aliphaticOctaveIndex = 3;
      mapping.aromaticNoteIndex = 5;
      mapping.aromaticOctaveIndex = 3;
      
      journey_mapping_B.setBackground(Color.LIGHT_GRAY);
      contemporary_mapping_B.setBackground(Color.LIGHT_GRAY);
      A_flat_major_6_mapping_B.setBackground(Color.LIGHT_GRAY);
      Diminished_D_mapping_B.setBackground(Color.LIGHT_GRAY);
      bass_ostinato_mapping_B.setBackground(Color.LIGHT_GRAY);
      raindrops_mapping_B.setBackground(Color.LIGHT_GRAY);
  }
  
   public int showSuggestedMappingDialog()
  { 
      centre();
      setVisible(true);
      return theButtonPressed;
  } 
   
    public static void main (String args[]) {
    new SuggestedMappingDialog (new javax.swing.JFrame (), true).show ();
  }

  
  private javax.swing.JPanel jPanel2;
  private javax.swing.JButton journey_mapping_B;
  private javax.swing.JButton contemporary_mapping_B;
  private javax.swing.JButton A_flat_major_6_mapping_B;
  private javax.swing.JButton Diminished_D_mapping_B;
  private javax.swing.JButton bass_ostinato_mapping_B;
  private javax.swing.JButton raindrops_mapping_B;
  private javax.swing.JButton cancelButton;
  private javax.swing.JButton save_and_close_Button;
  private javax.swing.JButton resetButton;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JScrollPane jScrollPane1;
  // End of variables declaration//GEN-END:variables
	void centre() 
	{
          	pack();

          	Point p = parent.getLocation();
          	Dimension d = parent.getSize();
          	Dimension s = getSize();

          	p.translate((d.width - s.width) / 2,
                      (d.height - s.height) / 2);
          	setLocation(p);
      	}
  
}
