import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;

import javax.swing.UIManager.*;



public class CustomMappingDialog extends javax.swing.JDialog { 

  private int     theButtonPressed = -1;
  private boolean buttonPressed = false;
  private Frame parent;
  Mapping mapping = new Mapping();
  ConversionCore theConversionCore = new ConversionCore();

  // Initialise arrays of possible notes and octaves
  static String[] notes = {"A", "A# / B♭", "B", "C", "C# / D♭", "D", "D# / E♭", "E", "F", "F# / G♭", "G", "G# / A♭"};
  static String[] octaves = {"1", "2", "3", "4", "5", "6", "7"};
  // Initialise the JComponents
  static JComboBox a_options = new JComboBox(notes);
  static JComboBox c_options = new JComboBox(notes);
  static JComboBox g_options = new JComboBox(notes);
  static JComboBox t_options = new JComboBox(notes);
  static JComboBox a_octave = new JComboBox(octaves);
  static JComboBox c_octave = new JComboBox(octaves);
  static JComboBox g_octave = new JComboBox(octaves);
  static JComboBox t_octave = new JComboBox(octaves);
  
  static JComboBox polar_options = new JComboBox(notes);
  static JComboBox hydrophobic_options = new JComboBox(notes);
  static JComboBox charged_options = new JComboBox(notes);
  static JComboBox positive_options = new JComboBox(notes);
  static JComboBox aliphatic_options = new JComboBox(notes);
  static JComboBox aromatic_options = new JComboBox(notes);
  static JComboBox polar_octave = new JComboBox(octaves);
  static JComboBox hydrophobic_octave = new JComboBox(octaves);
  static JComboBox charged_octave = new JComboBox(octaves);
  static JComboBox positive_octave = new JComboBox(octaves);
  static JComboBox aliphatic_octave = new JComboBox(octaves);
  static JComboBox aromatic_octave = new JComboBox(octaves);
  
  static JButton resetButton = new JButton();
  
  // Initialise the labels
  JLabel a_label = new JLabel("A mapping", JLabel.CENTER);
  JLabel c_label = new JLabel("C mapping", JLabel.CENTER);
  JLabel g_label = new JLabel("G mapping", JLabel.CENTER);
  JLabel t_label = new JLabel("T mapping", JLabel.CENTER);
  
  JLabel polar_label = new JLabel("Polar", JLabel.CENTER);
  JLabel hydrophobic_label = new JLabel("Hydrophobic", JLabel.CENTER);
  JLabel charged_label = new JLabel("Charged", JLabel.CENTER);
  JLabel positive_label = new JLabel("Positive", JLabel.CENTER);
  JLabel aliphatic_label = new JLabel("Aliphatic", JLabel.CENTER);
  JLabel aromatic_label = new JLabel("Aromatic", JLabel.CENTER);
 
  /** Creates new form CustomMappingDialog */
  public CustomMappingDialog(java.awt.Frame theParent, boolean modal) {
    super (theParent, modal);
    parent = theParent;
    initLookAndFeel(); 
    initComponents ();
    pack ();
  }
  
  // Set Nimbus Look and Feel
  private static void initLookAndFeel() 
   {
       try 
       {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) 
            {
                if ("Nimbus".equals(info.getName())) 
                {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
       }
       catch (Exception e) {}
  }
  
  // Initilise all components and corresponding handlers
  public void initComponents () 
  {
    jPanel2 = new javax.swing.JPanel ();
    jSeparator1 = new javax.swing.JSeparator ();
    resetButton = new javax.swing.JButton ();
    cancelButton = new javax.swing.JButton ();
    save_and_close_Button = new javax.swing.JButton();
    jPanel1 = new javax.swing.JPanel ();
  
    setTitle ("Change mappings for nucleotides and amino acids");
    setResizable (false);
    addWindowListener (new java.awt.event.WindowAdapter () {
      public void windowClosing (java.awt.event.WindowEvent evt) {
        closeDialog (evt);
      }
    }
    );
    
    // BEGIN INITIALISING JCOMBOBOXES
    
    jPanel2.setLayout (new java.awt.GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    
    a_label.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 0;
    c.gridy = 0;
    
    jPanel2.add(a_label, c);
    
    a_options.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 0;
    c.gridy = 1;
    a_options.setSelectedIndex(0);
    a_options.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          a_mapping (evt);
        }
      }
      );
    
    jPanel2.add(a_options, c);
    
    a_octave.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 0;
    c.gridy = 2;
    a_octave.setSelectedIndex(4);
    a_octave.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          a_octave_handler (evt);
        }
      }
      );
    
    jPanel2.add(a_octave, c);
    
    
    c_label.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 1;
    c.gridy = 0;
    
    jPanel2.add(c_label, c);
      
    c_options.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 1;
    c.gridy = 1;
    c_options.setSelectedIndex(10);
    c_options.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          c_mapping (evt);
        }
      }
      );
    
    jPanel2.add(c_options, c);
    
    c_octave.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 1;
    c.gridy = 2;
    c_octave.setSelectedIndex(4);
    c_octave.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          c_octave_handler (evt);
        }
      }
      );
    
    jPanel2.add(c_octave, c);
      
    g_label.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 2;
    c.gridy = 0;
    
    jPanel2.add(g_label, c);

    g_options.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 2;
    c.gridy = 1;
    g_options.setSelectedIndex(3);
    g_options.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          g_mapping (evt);
        }
      }
      );
    
    jPanel2.add(g_options, c);
    
    g_octave.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 2;
    c.gridy = 2;
    g_octave.setSelectedIndex(4);
    g_octave.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          g_octave_handler (evt);
        }
      }
      );
    
    jPanel2.add(g_octave, c);
      
    
    t_label.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 3;
    c.gridy = 0;
    
    jPanel2.add(t_label, c);
    
    t_options.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 3;
    c.gridy = 1;
    t_options.setSelectedIndex(7);
    t_options.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          t_mapping (evt);
        }
      }
      );
    
    jPanel2.add(t_options, c);
    
    
    t_octave.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 3;
    c.gridy = 2;
    t_octave.setSelectedIndex(4);
    t_octave.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          t_octave_handler (evt);
        }
      }
      );
    
    jPanel2.add(t_octave, c);
    
    polar_label.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.insets = new Insets(25,0,0,0);  //top padding
    c.gridx = 0;
    c.gridy = 3;
    
    jPanel2.add(polar_label, c);
    
    polar_options.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.insets = new Insets(0,0,0,0);
    c.gridx = 0;
    c.gridy = 4;
    polar_options.setSelectedIndex(0);
    polar_options.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          polar_mapping (evt);
        }
      }
      );
    
    jPanel2.add(polar_options, c);
    
    polar_octave.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 0;
    c.gridy = 5;
    polar_octave.setSelectedIndex(2);
    polar_octave.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          polar_octave_handler (evt);
        }
      }
      );
    
    jPanel2.add(polar_octave, c);
    
    hydrophobic_label.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.insets = new Insets(25,0,0,0);
    c.gridx = 1;
    c.gridy = 3;
    
    jPanel2.add(hydrophobic_label, c);
      
    hydrophobic_options.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.insets = new Insets(0,0,0,0);
    c.gridx = 1;
    c.gridy = 4;
    hydrophobic_options.setSelectedIndex(3);
    hydrophobic_options.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          hydrophobic_mapping (evt);
        }
      }
      );
    
    jPanel2.add(hydrophobic_options, c);
    
    hydrophobic_octave.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 1;
    c.gridy = 5;
    hydrophobic_octave.setSelectedIndex(2);
    hydrophobic_octave.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          hydrophobic_octave_handler (evt);
        }
      }
      );
    
    jPanel2.add(hydrophobic_octave, c);
    
    charged_label.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.insets = new Insets(25,0,0,0);
    c.gridx = 2;
    c.gridy = 3;
    
    jPanel2.add(charged_label, c);

    charged_options.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.insets = new Insets(0,0,0,0);
    c.gridx = 2;
    c.gridy = 4;
    charged_options.setSelectedIndex(8);
    charged_options.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          charged_mapping (evt);
        }
      }
      );
    
    jPanel2.add(charged_options, c);
    
    charged_octave.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 2;
    c.gridy = 5;
    charged_octave.setSelectedIndex(2);
    charged_octave.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          charged_octave_handler (evt);
        }
      }
      );
    
    jPanel2.add(charged_octave, c);
    
    positive_label.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.insets = new Insets(25,0,0,0);
    c.gridx = 3;
    c.gridy = 3;
    
    jPanel2.add(positive_label, c);
    
    positive_options.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.insets = new Insets(0,0,0,0);
    c.gridx = 3;
    c.gridy = 4;
    positive_options.setSelectedIndex(7);
    positive_options.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          positive_mapping (evt);
        }
      }
      );
    
    jPanel2.add(positive_options, c);
    
    positive_octave.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 3;
    c.gridy = 5;
    positive_octave.setSelectedIndex(2);
    positive_octave.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          positive_octave_handler (evt);
        }
      }
      );
    
    jPanel2.add(positive_octave, c);
    
    aliphatic_label.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 1;
    c.gridy = 6;
    
    jPanel2.add(aliphatic_label, c);
    
    aliphatic_options.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 1;
    c.gridy = 7;
    aliphatic_options.setSelectedIndex(10);
    aliphatic_options.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          aliphatic_mapping (evt);
        }
      }
      );
    
    jPanel2.add(aliphatic_options, c);
    
    aliphatic_octave.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 1;
    c.gridy = 8;
    aliphatic_octave.setSelectedIndex(2);
    aliphatic_octave.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          aliphatic_octave_handler (evt);
        }
      }
      );
    
    jPanel2.add(aliphatic_octave, c);
    
    aromatic_label.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 2;
    c.gridy = 6;
    
    jPanel2.add(aromatic_label, c);
    
    aromatic_options.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 2;
    c.gridy = 7;
    aromatic_options.setSelectedIndex(5);
    aromatic_options.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          aromatic_mapping (evt);
        }
      }
      );
    
    jPanel2.add(aromatic_options, c);
    
    aromatic_octave.setPreferredSize(new Dimension(120, 20));
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 2;
    c.gridy = 8;
    aromatic_octave.setSelectedIndex(2);
    aromatic_octave.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          aromatic_octave_handler (evt);
        }
      }
      );
    
    jPanel2.add(aromatic_octave, c);
    
      save_and_close_Button.setPreferredSize(new Dimension(120, 35));
      save_and_close_Button.setText ("Save & Close");
      c.fill = GridBagConstraints.HORIZONTAL;
      c.insets = new Insets(30,0,0,0);
      c.gridx = 0;
      c.gridy = 9;
      save_and_close_Button.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          save_and_close_ButtonPerformed (evt);
        }
      }
      );
  
      jPanel2.add (save_and_close_Button, c);
      
      resetButton.setPreferredSize(new Dimension(120, 35));
      resetButton.setText ("Reset");
      c.fill = GridBagConstraints.HORIZONTAL;
      c.gridx = 1;
      c.gridy = 9;
      resetButton.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          resetButtonPerformed (evt);
        }
      }
      );
  
      jPanel2.add (resetButton, c);
      
      cancelButton.setPreferredSize(new Dimension(120, 35));
      cancelButton.setText ("Cancel");
      c.fill = GridBagConstraints.HORIZONTAL;
      c.gridx = 3;
      c.gridy = 9;
      cancelButton.addActionListener (new java.awt.event.ActionListener () {
        public void actionPerformed (java.awt.event.ActionEvent evt) {
          cancelButtonPerformed (evt);
        }
      }
      );
  
      jPanel2.add (cancelButton, c);
    
      jPanel1.setBorder(new javax.swing.border.TitledBorder("Please select the new mappings for the top line and bass line"));
      getContentPane ().add (jPanel1, java.awt.BorderLayout.NORTH);
      getContentPane().add(jPanel2, java.awt.BorderLayout.SOUTH);

  }
  
  // SET THE HANDLERS FOR EACH COMBO BOX
  
  private void a_mapping (java.awt.event.ActionEvent evt) {
    // Add your handling code here:  
    JComboBox nuc = (JComboBox)evt.getSource();
    String current = (String)nuc.getSelectedItem();
    
    int i = 0;
    while (i < notes.length)
    {
        if (current.equals(notes[i]))
            mapping.nucANoteIndex = i;
        i++;
    }
  }
  
  private void c_mapping (java.awt.event.ActionEvent evt) {
    // Add your handling code here:
    
    JComboBox nuc = (JComboBox)evt.getSource();
    String current = (String)nuc.getSelectedItem();
    
    int i = 0;
    while (i < notes.length)
    {
        if (current.equals(notes[i]))
            mapping.nucCNoteIndex = i;
        i++;
    }
  }
  
  private void g_mapping (java.awt.event.ActionEvent evt) {
    // Add your handling code here:
    
    JComboBox nuc = (JComboBox)evt.getSource();
    String current = (String)nuc.getSelectedItem();
    
    int i = 0;
    while (i < notes.length)
    {
        if (current.equals(notes[i]))
            mapping.nucGNoteIndex = i;
        i++;
    }
  }
  
  private void t_mapping (java.awt.event.ActionEvent evt) {
    // Add your handling code here:
    
    JComboBox nuc = (JComboBox)evt.getSource();
    String current = (String)nuc.getSelectedItem();
    
    int i = 0;
    while (i < notes.length)
    {
        if (current.equals(notes[i]))
            mapping.nucTNoteIndex = i;
        i++;
    }
  }
  
  private void a_octave_handler (java.awt.event.ActionEvent evt) {
      // INSERT CODE HERE
      JComboBox oct = (JComboBox)evt.getSource();
      String current = (String)oct.getSelectedItem();
      
    int i = 0;
    while (i < octaves.length)
    {
        if (current.equals(octaves[i]))
            mapping.nucAOctaveIndex = i + 1;
        i++;
    }

  }
  
  private void c_octave_handler (java.awt.event.ActionEvent evt) {
      // INSERT CODE HERE
      JComboBox oct = (JComboBox)evt.getSource();
      String current = (String)oct.getSelectedItem();
      
    int i = 0;
    while (i < octaves.length)
    {
        if (current.equals(octaves[i]))
            mapping.nucCOctaveIndex = i + 1;
        i++;
    }
  }
  
  private void g_octave_handler (java.awt.event.ActionEvent evt) {
      // INSERT CODE HERE
      JComboBox oct = (JComboBox)evt.getSource();
      String current = (String)oct.getSelectedItem();
      
    int i = 0;
    while (i < octaves.length)
    {
        if (current.equals(octaves[i]))
            mapping.nucGOctaveIndex = i + 1;
        i++;
    }
  }
  
  private void t_octave_handler (java.awt.event.ActionEvent evt) {
      // INSERT CODE HERE
      JComboBox oct = (JComboBox)evt.getSource();
      String current = (String)oct.getSelectedItem();

    int i = 0;
    while (i < octaves.length)
    {
        if (current.equals(octaves[i]))
            mapping.nucTOctaveIndex = i + 1;
        i++;
    }
  }
  
  private void polar_mapping (java.awt.event.ActionEvent evt) {
    // Add your handling code here:
    
    JComboBox nuc = (JComboBox)evt.getSource();
    String current = (String)nuc.getSelectedItem();
    
    int i = 0;
    while (i < notes.length)
    {
        if (current.equals(notes[i]))
            mapping.polarNoteIndex = i;
        i++;
    }
  }
  
  private void hydrophobic_mapping (java.awt.event.ActionEvent evt) {
    // Add your handling code here:
    
    JComboBox nuc = (JComboBox)evt.getSource();
    String current = (String)nuc.getSelectedItem();
    
    int i = 0;
    while (i < notes.length)
    {
        if (current.equals(notes[i]))
            mapping.hydroNoteIndex = i;
        i++;
    }
  }
  
  private void charged_mapping (java.awt.event.ActionEvent evt) {
    // Add your handling code here:
    
    JComboBox nuc = (JComboBox)evt.getSource();
    String current = (String)nuc.getSelectedItem();
    
    int i = 0;
    while (i < notes.length)
    {
        if (current.equals(notes[i]))
            mapping.chargedNoteIndex = i;
        i++;
    }

  }
  
  private void positive_mapping (java.awt.event.ActionEvent evt) {
    // Add your handling code here:
    
    JComboBox nuc = (JComboBox)evt.getSource();
    String current = (String)nuc.getSelectedItem();
    
    int i = 0;
    while (i < notes.length)
    {
        if (current.equals(notes[i]))
            mapping.positiveNoteIndex = i;
        i++;
    }

  }
  
  private void aliphatic_mapping (java.awt.event.ActionEvent evt) {
    // Add your handling code here:
    
    JComboBox nuc = (JComboBox)evt.getSource();
    String current = (String)nuc.getSelectedItem();
    
    int i = 0;
    while (i < notes.length)
    {
        if (current.equals(notes[i]))
            mapping.aliphaticNoteIndex = i;
        i++;
    }

  }
  
  private void aromatic_mapping (java.awt.event.ActionEvent evt) {
    // Add your handling code here:
    
    JComboBox nuc = (JComboBox)evt.getSource();
    String current = (String)nuc.getSelectedItem();
    
    int i = 0;
    while (i < notes.length)
    {
        if (current.equals(notes[i]))
            mapping.aromaticNoteIndex = i;
        i++;
    }

  }
  
  private void polar_octave_handler (java.awt.event.ActionEvent evt) {
      // INSERT CODE HERE
    JComboBox oct = (JComboBox)evt.getSource();
    String current = (String)oct.getSelectedItem();
      
    int i = 0;
    while (i < octaves.length)
    {
        if (current.equals(octaves[i]))
            mapping.polarOctaveIndex = i + 1;
        i++;
    }
  }
  
  private void hydrophobic_octave_handler (java.awt.event.ActionEvent evt) {
      // INSERT CODE HERE
      JComboBox oct = (JComboBox)evt.getSource();
      String current = (String)oct.getSelectedItem();
      
    int i = 0;
    while (i < octaves.length)
    {
        if (current.equals(octaves[i]))
            mapping.hydroOctaveIndex = i + 1;
        i++;
    }
  }
  
  private void charged_octave_handler (java.awt.event.ActionEvent evt) {
      // INSERT CODE HERE
      JComboBox oct = (JComboBox)evt.getSource();
      String current = (String)oct.getSelectedItem();
      
    int i = 0;
    while (i < octaves.length)
    {
        if (current.equals(octaves[i]))
            mapping.chargedOctaveIndex = i + 1;
        i++;
    }
  }
  
  private void positive_octave_handler (java.awt.event.ActionEvent evt) {
      // INSERT CODE HERE
      JComboBox oct = (JComboBox)evt.getSource();
      String current = (String)oct.getSelectedItem();
      
    int i = 0;
    while (i < octaves.length)
    {
        if (current.equals(octaves[i]))
            mapping.positiveOctaveIndex = i + 1;
        i++;
    }
  }
  
  private void aliphatic_octave_handler (java.awt.event.ActionEvent evt) {
      // INSERT CODE HERE
      JComboBox oct = (JComboBox)evt.getSource();
      String current = (String)oct.getSelectedItem();
      
    int i = 0;
    while (i < octaves.length)
    {
        if (current.equals(octaves[i]))
            mapping.aliphaticOctaveIndex = i + 1;
        i++;
    }
  }
  
  private void aromatic_octave_handler (java.awt.event.ActionEvent evt) {
      // INSERT CODE HERE
      JComboBox oct = (JComboBox)evt.getSource();
      String current = (String)oct.getSelectedItem();
      
    int i = 0;
    while (i < octaves.length)
    {
        if (current.equals(octaves[i]))
            mapping.aromaticOctaveIndex = i + 1;
        i++;
    }
  }
  
  private void save_and_close_ButtonPerformed (java.awt.event.ActionEvent evt) {
    theButtonPressed = 1;
    buttonPressed = true;
    mapping.key = 0;
    setVisible(false);
  }
  
    private void cancelButtonPerformed (java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonPerformed
    // Add your handling code here:
    resetAll();
    theButtonPressed = 0;
    setVisible(false);
  }
    
    private void resetButtonPerformed (java.awt.event.ActionEvent evt) {
    resetAll();
    theButtonPressed = 1;
    buttonPressed = true;
  }
    
  
    /** Closes the dialog */
  private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
    setVisible (false);
    dispose ();
  }//GEN-LAST:event_closeDialog
  
  // reset all indexes to default values - also resets the musical score
  public void resetAll()
  {
    a_options.setSelectedIndex(0);
    c_options.setSelectedIndex(10);
    g_options.setSelectedIndex(3);
    t_options.setSelectedIndex(7);
    
    a_octave.setSelectedIndex(4);
    c_octave.setSelectedIndex(4);
    g_octave.setSelectedIndex(4);
    t_octave.setSelectedIndex(4);
   
    polar_options.setSelectedIndex(0);
    hydrophobic_options.setSelectedIndex(3);
    charged_options.setSelectedIndex(8);
    positive_options.setSelectedIndex(7);
    aliphatic_options.setSelectedIndex(10);
    aromatic_options.setSelectedIndex(5);
    
    polar_octave.setSelectedIndex(2);
    hydrophobic_octave.setSelectedIndex(2);
    charged_octave.setSelectedIndex(2);
    positive_octave.setSelectedIndex(2);
    aliphatic_octave.setSelectedIndex(2);
    aromatic_octave.setSelectedIndex(2);

  }
  
   public int showCustomMappingDialog()
  { 
      centre();
      setVisible(true);
      return theButtonPressed;
  }
  
  
   
   
    public static void main (String args[]) {
    new CustomMappingDialog (new javax.swing.JFrame (), true).show ();
  }

  
  private javax.swing.JPanel jPanel2;
  private javax.swing.JButton cancelButton;
  private javax.swing.JButton save_and_close_Button;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JSeparator jSeparator1;
  private javax.swing.JScrollPane jScrollPane1;
  // End of variables declaration//GEN-END:variables
	void centre() 
	{
          	pack();

          	Point p = parent.getLocation();
          	Dimension d = parent.getSize();
          	Dimension s = getSize();

          	p.translate((d.width - s.width) / 2,
                      (d.height - s.height) / 2);
          	setLocation(p);
      	}
  
}
