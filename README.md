#Welcome to Protein Music!

This program was originally created by Ross King and Colin Angus in 1996 for the purpose of audifying input protein 
encoded DNA sequences. The result is a musical track that can be played within the program and converted into MIDI format
for playback on a wide variety of devices. A full MIDI instrument board is available within the program, as well as
playback control.

The program was extended by Stas Shvetsov (University of Manchester) in 2016 as the aim of his third year project. It now
contains the following additional features:

- Graphical improvements
- Dynamic musical score
- Dialog box for the creation of custom mappings for nucleotides and amino acid properties
- Dialog box containing fully-implemented, suggested mappings for this program
- Key shift function, covering a range of major and minor keys
- Info panel containing current key information
- 'Make Whole' function, used to set sharp keys to whole keys when defining custom mappings
- General enhancements (info for paste dialog box, text updates, etc.) 

--------------------------------------------------------------------------------------------------------------------------

**HOW TO RUN >>>>>**

Java must be installed and it is recommended to use the latest version (currently 8.0.1020.14).

MacOS/Linux Terminal commands:

1. cd Protein_Music/dist
2. java -jar Protein_Music.jar

Windows:

1. Navigate to Protein_Music/dist folder
2. Launch Protein_Music.jar

---------------------------------------------------------------------------------------------------------------------------

**GETTING STARTED with PROTEIN MUSIC >>>>>**

It is advisable to read the ProteinMusic.pdf provided in the docs folders. This document, created by Ross King and
Colin Angus, contains information about the biology behind Protein Music and will give you a deeper insight into
the processes behind the program.

Once you have launched the program, begin by navigating to File > Open Project and selecting a sequence for audification.
The folder 'sequences' provided will contain a variety of sequences to get you started.
More sequences can be found on the European Nucleotide Archive (ENA): http://www.ebi.ac.uk/ena

Naturally, the first thing you will want to do once you have loaded your protein encoded DNA sequence is to play it. 
Play around with varying mappings to find a sound that works best for you. The default mapping of the program is based
around C Major.
If you are unsure of the sound you are looking for, the suggested mappings may be a good place to start.

Below is a brief outline of what you can do with Protein Music:

- **Play ->** Plays the selected sequence.
- **Pause ->** Stops the sequence playback. Play will resume the sequence from this point.
- **Rewind & Forward ->** Rewinds or fast-forwards the sequence.
- **Tempo Slider ->** Alters the tempo of the sequence (greater BPM = greater playback speed)
- **Instrument Board ->** A full MIDI instrument board, containing 128 different instruments. Top line and bass line can be 
					set separately via the selection box above the board.				
- **Key Up ->** Shifts the program mapping up one whole key (e.g C Major -> D Major) 
- **Key Down ->** Shifts the program mapping down one whole key (e.g C Major -> B Major)
- **Major/Minor Switch ->** Shifts the program from the major key to it's corresponding minor key, and vice versa
					  (e.g C Major -> C Minor || G Minor -> G Major)
- **Reset ->** Resets the program back to it's default state and resets all mappings changes.
- **Edit > Paste Sequence ->** Paste your own sequences for audification (must be in an approved format)
- **Edit > Set Custom Mapping ->** Select musical note and octave mappings for each of the four nucleotides, and each
							  of the six amino acid properties.
- **Edit > Suggested Mapping ->** A set of fully-implemented suggested mappings for this program. These mappings have been
							  carefully acquired using good musical theory practices.
- **Make Whole ->** Used to simultaneously shift all sharp keys up or down a half tone (with 50/50 probability) when
				a custom mapping has been implemented.
- **File > Save MIDI File/Save MIDI File as ->** Save the musical track from the sequence (complete with any mapping changes) 
											 to a MIDI file.
					 
If you have any further questions or queries, feel free to contact me at: 

stazio08@gmail.com (or alternatively, stanislav.shvetsov@student.manchester.ac.uk)

Now that this program has been made open-source, if you wish to make any changes or implement further features, you are actively encouraged to do so.