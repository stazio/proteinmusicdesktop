/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Stazio
 */
import org.junit.Test;
import static org.junit.Assert.*;

public class MusicalNoteTest {
    MusicalNote musString = new MusicalNote("A5");
    MusicalNote musInt = new MusicalNote(69);
    
    @Test
    public void convertFromIntegerTest() {
        // Test whole notes on varying octaves
        assertEquals("MusicalNote A5 is equal to it's integer representation of 69", "A5", musInt.convertFromInteger(69));
        assertEquals("MusicalNote A4 is equal to it's integer representation of 57", "A4", musInt.convertFromInteger(57));
        assertEquals("MusicalNote A6 is equal to it's integer representation of 81", "A6", musInt.convertFromInteger(81));
        // Random whole note choices
        assertEquals("MusicalNote B2 is equal to it's integer representation of 35", "B2", musInt.convertFromInteger(35));
        assertEquals("MusicalNote C7 is equal to it's integer representation of 84", "C7", musInt.convertFromInteger(84));  
        // Test a few sharp notes
        assertEquals("MusicalNote D#5 is equal to it's integer representation of 63", "d5", musInt.convertFromInteger(63));
        assertEquals("MusicalNote G#3 is equal to it's integer representation of 44", "g3", musInt.convertFromInteger(44));
    }
    
    @Test
    public void convertFromStringTest() {
        // Test whole notes on varying octaves
        assertEquals("MusicalNote 69 is equal to it's string representation of A5", 69, musString.convertFromString("A5"));
        assertEquals("MusicalNote 57 is equal to it's string representation of A4", 57, musString.convertFromString("A4"));
        assertEquals("MusicalNote 81 is equal to it's string representation of A6", 81, musString.convertFromString("A6"));
        // Random whole note choices
        assertEquals("MusicalNote 35 is equal to it's string representation of B2", 35, musString.convertFromString("B2"));
        assertEquals("MusicalNote 84 is equal to it's string representation of C7", 84, musString.convertFromString("C7"));  
        // Test a few sharp notes
        assertEquals("MusicalNote 63 is equal to it's string representation of D#5", 63, musString.convertFromString("d5"));
        assertEquals("MusicalNote 44 is equal to it's string representation of G#3", 44, musString.convertFromString("g3"));
    }
    
    @Test
    public void getIntegerRepresentationTest() {
        assertEquals("Test for correct integer representation return", 69, musString.getIntegerRepresentation());
    }
    
    @Test
    public void getStringRepresentationTest() {
        assertEquals("Test for correct string representation return", "A5", musInt.getStringRepresentation());
    }
}
 