/**
 *
 * @author Stazio
 */
import org.junit.Test;
import static org.junit.Assert.*;

public class OctaveChangeTest {
   
    // Test this method for a small octave change (+1) and a large octave change (+6)
    @Test
    public void convertNoteTest(){
        OctaveChange oct1 = new OctaveChange(1);
        OctaveChange oct6 = new OctaveChange(6);
        MusicalNote test_note1 = new MusicalNote(69);
        MusicalNote expected_note1 = new MusicalNote(81);
        MusicalNote test_note6 = new MusicalNote(21);
        MusicalNote expected_note6 = new MusicalNote(93);
        
        assertEquals("Octave shift by one octave", expected_note1.getIntegerRepresentation(), oct1.convertNote(test_note1).getIntegerRepresentation());
        assertEquals("Octave shift by one octave", expected_note6.getIntegerRepresentation(), oct6.convertNote(test_note6).getIntegerRepresentation());
    }
}
