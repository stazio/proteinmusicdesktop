/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Stazio
 */
import org.junit.Test;
import static org.junit.Assert.*;

public class MappingTest {
  
    Mapping testMapping = new Mapping();
    
    // Test the note to be mapped
    @Test
    public void theNoteToBeMappedTest() {
        assertEquals("Note index 2 is mapped to note B", "B", testMapping.theNoteToBePlayed(2));
    }
    // Test the octave to be mapped
    @Test
    public void theOctaveToBeMappedTest() {
        assertEquals("Octave index 2 is mapped to octave 2", "2", testMapping.theOctaveToBePlayed(2));
    }
    // Test for the correct nuc mapping returned
    @Test
    public void getNucMappingTest() {
        assertEquals("Char a in sequence maps to NucA", testMapping.nucA, testMapping.getNucMapping("a"));
    }
    
}
