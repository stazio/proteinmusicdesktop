/**
 *
 * @author Stazio
 */
import org.junit.Test;
import static org.junit.Assert.*;

public class CodonTest {
    
    @Test
    public void getAminoAcidTimingTest(){
        // initialise dummy codon information
        MusicalNote dummyNote = new MusicalNote("A5");
        Codon[] codons = new Codon[5];
        String[] dummyStrings = new String[6];
        dummyStrings[0] = "TTT";
        dummyStrings[1] = "TTC";
        dummyStrings[2] = "";
        dummyStrings[3] = "";
        dummyStrings[4] = "";
        dummyStrings[5] = "";
        codons[0] = new Codon(dummyStrings,"f");
        codons[0].addAminoAcidTiming(new AminoAcidTimeAttributes(dummyNote,0,0,0,0));
        codons[0].addAminoAcidTiming(new AminoAcidTimeAttributes(new MusicalNote("D5"),4,2,1,0));
        codons[0].setSize(CodonConstants.LARGE);
        dummyStrings[0] = "TTA";
        dummyStrings[1] = "TTG";
        dummyStrings[2] = "CTT";
        dummyStrings[3] = "CTC";
        dummyStrings[4] = "CTA";
        dummyStrings[5] = "CTG";
        codons[1] = new Codon(dummyStrings,"l");
        codons[1].addAminoAcidTiming(new AminoAcidTimeAttributes(dummyNote,0,0,0,0));
        codons[1].addAminoAcidTiming(new AminoAcidTimeAttributes(new MusicalNote("D5"),4,2,1,0));
        codons[1].setSize(CodonConstants.LARGE);
        AminoAcidTimeAttributes dummyAminoAcidTimeAttributes = new AminoAcidTimeAttributes(dummyNote,0,0,0,0);
        // Assert equals by individual property
        assertEquals("Check for the same return by play time", codons[0].getAminoAcidTiming(0).getPlayTime(), codons[1].getAminoAcidTiming(0).getPlayTime());
        assertEquals("Check for the same return by duration time", codons[0].getAminoAcidTiming(0).getDurationTime(), codons[1].getAminoAcidTiming(0).getDurationTime());
        assertEquals("Check for the same return by velocity on time", codons[0].getAminoAcidTiming(0).getVelocityOnTime(), codons[1].getAminoAcidTiming(0).getVelocityOnTime());
        assertEquals("Check for the same return by velocity off time", codons[0].getAminoAcidTiming(0).getVelocityOffTime(), codons[1].getAminoAcidTiming(0).getVelocityOffTime());
    }
}
